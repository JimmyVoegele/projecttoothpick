﻿namespace ToothPick
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem24 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem25 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem26 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem27 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem28 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem29 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem30 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem31 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem32 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem33 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem34 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem35 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem36 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem37 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem38 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem39 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem40 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem41 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem42 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem43 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem44 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem45 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem46 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem47 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem48 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem49 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem50 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem51 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem52 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlSchoolYear = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.txtPIN = new Telerik.WinControls.UI.RadTextBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.lblDataRefreshTime = new Telerik.WinControls.UI.RadLabel();
            this.btnRefreshSchools = new Telerik.WinControls.UI.RadButton();
            this.btnRefreshStudents = new Telerik.WinControls.UI.RadButton();
            this.cmbSchools = new Telerik.WinControls.UI.RadDropDownList();
            this.schoolsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.schoolsDataset = new ToothPick.SchoolsDataset();
            this.lblSchool = new Telerik.WinControls.UI.RadLabel();
            this.btnFullScreen = new Telerik.WinControls.UI.RadButton();
            this.pnlChildSearch = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.chkLunchTimeColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkBColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkYColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkRColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkCommentsColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkTeacherFirstColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHomeRoomColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkSpecialTimeColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkRecessColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGradeColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGenderColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDOBColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkLanguageColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkTeacherLastColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.chkFirstNameColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtAfterSchoolDDS = new Telerik.WinControls.UI.RadTextBox();
            this.txtAfterSchoolHyg = new Telerik.WinControls.UI.RadTextBox();
            this.txtAfterSchoolCustom = new Telerik.WinControls.UI.RadTextBox();
            this.chkAfterSchoolCustom = new Telerik.WinControls.UI.RadCheckBox();
            this.chkAfterSchoolReferralConsult = new Telerik.WinControls.UI.RadCheckBox();
            this.chkAfterSchoolDDS = new Telerik.WinControls.UI.RadCheckBox();
            this.chkAfterSchoolHyg = new Telerik.WinControls.UI.RadCheckBox();
            this.chkAfterSchool = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.chkComplexPatientWheelchair = new Telerik.WinControls.UI.RadCheckBox();
            this.txtComplexPatientCustom = new Telerik.WinControls.UI.RadTextBox();
            this.chkComplexPatientCustom = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatientSpecialNeeds = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatientGagReflex = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatientLimitedOpening = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatientAnesthetic = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatientBehavior = new Telerik.WinControls.UI.RadCheckBox();
            this.chkComplexPatient = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtHygienistSealant = new Telerik.WinControls.UI.RadTextBox();
            this.txtHygienistCustom = new Telerik.WinControls.UI.RadTextBox();
            this.chkHygienistCustom = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienistSealant = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienistPX2 = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienistPX1 = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienistHygieneFirst = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienistFluoride = new Telerik.WinControls.UI.RadCheckBox();
            this.chkHygienist = new Telerik.WinControls.UI.RadCheckBox();
            this.frpDentist = new Telerik.WinControls.UI.RadGroupBox();
            this.txtDentistCustom = new Telerik.WinControls.UI.RadTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDentistLRAnes = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistCustom = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistLLAnes = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistIntake = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistULAnes = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistLR = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistURAnes = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistLL = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistUL = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentistUR = new Telerik.WinControls.UI.RadCheckBox();
            this.chkDentist = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGrade5th = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGrade4th = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGrade3rd = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGrade2nd = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGrade1st = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGradeK = new Telerik.WinControls.UI.RadCheckBox();
            this.chkGradeAll = new Telerik.WinControls.UI.RadCheckBox();
            this.lblFilterGrade = new Telerik.WinControls.UI.RadLabel();
            this.cmbStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.lblFilterStatus = new Telerik.WinControls.UI.RadLabel();
            this.pnlStudentPickList = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.studentTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.studentDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.studentDataSet = new ToothPick.StudentDataSet();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblStudentCount = new Telerik.WinControls.UI.RadLabel();
            this.btnClearSort = new Telerik.WinControls.UI.RadButton();
            this.btnSort2 = new Telerik.WinControls.UI.RadToggleButton();
            this.ddlSort2 = new Telerik.WinControls.UI.RadDropDownList();
            this.btnSort3 = new Telerik.WinControls.UI.RadToggleButton();
            this.ddlSort3 = new Telerik.WinControls.UI.RadDropDownList();
            this.btnSort1 = new Telerik.WinControls.UI.RadToggleButton();
            this.ddlSort1 = new Telerik.WinControls.UI.RadDropDownList();
            this.lblSort = new Telerik.WinControls.UI.RadLabel();
            this.schoolsTableAdapter = new ToothPick.SchoolsDatasetTableAdapters.SchoolsTableAdapter();
            this.studentTableTableAdapter = new ToothPick.StudentDataSetTableAdapters.StudentTableTableAdapter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chkLunchColumnVisible = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSchoolYear)).BeginInit();
            this.pnlSchoolYear.PanelContainer.SuspendLayout();
            this.pnlSchoolYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataRefreshTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefreshSchools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefreshStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSchools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSchool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFullScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlChildSearch)).BeginInit();
            this.pnlChildSearch.PanelContainer.SuspendLayout();
            this.pnlChildSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLunchTimeColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommentsColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTeacherFirstColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHomeRoomColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpecialTimeColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecessColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGenderColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDOBColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLanguageColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTeacherLastColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstNameColumnVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolDDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolHyg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolReferralConsult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolDDS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolHyg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientWheelchair)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComplexPatientCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientSpecialNeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientGagReflex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientLimitedOpening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientAnesthetic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientBehavior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHygienistSealant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHygienistCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistSealant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistPX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistPX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistHygieneFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistFluoride)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frpDentist)).BeginInit();
            this.frpDentist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDentistCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLRAnes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistCustom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLLAnes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistIntake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistULAnes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistURAnes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistUL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistUR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade5th)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade4th)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade3rd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade2nd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade1st)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilterGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilterStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlStudentPickList)).BeginInit();
            this.pnlStudentPickList.PanelContainer.SuspendLayout();
            this.pnlStudentPickList.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblStudentCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLunchColumnVisible)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSchoolYear
            // 
            this.pnlSchoolYear.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSchoolYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlSchoolYear.HeaderText = "School Information";
            this.pnlSchoolYear.Location = new System.Drawing.Point(0, 0);
            this.pnlSchoolYear.Name = "pnlSchoolYear";
            this.pnlSchoolYear.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 1253, 79);
            // 
            // pnlSchoolYear.PanelContainer
            // 
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.txtPIN);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.lblVersion);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.lblDataRefreshTime);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.btnRefreshSchools);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.btnRefreshStudents);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.cmbSchools);
            this.pnlSchoolYear.PanelContainer.Controls.Add(this.lblSchool);
            this.pnlSchoolYear.PanelContainer.Size = new System.Drawing.Size(1247, 50);
            this.pnlSchoolYear.Size = new System.Drawing.Size(1249, 79);
            this.pnlSchoolYear.TabIndex = 4;
            this.pnlSchoolYear.Expanded += new System.EventHandler(this.pnlSchoolYear_Expanded);
            this.pnlSchoolYear.Collapsed += new System.EventHandler(this.pnlSchoolYear_Collapsed);
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(140, 13);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.PasswordChar = '*';
            this.txtPIN.Size = new System.Drawing.Size(379, 20);
            this.txtPIN.TabIndex = 0;
            this.txtPIN.TextChanged += new System.EventHandler(this.txtPIN_TextChanged);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblVersion.Location = new System.Drawing.Point(1193, 11);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 25);
            this.lblVersion.TabIndex = 5;
            this.lblVersion.Text = "v 1.6";
            this.lblVersion.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblDataRefreshTime
            // 
            this.lblDataRefreshTime.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblDataRefreshTime.Location = new System.Drawing.Point(767, 11);
            this.lblDataRefreshTime.Name = "lblDataRefreshTime";
            this.lblDataRefreshTime.Size = new System.Drawing.Size(257, 25);
            this.lblDataRefreshTime.TabIndex = 3;
            this.lblDataRefreshTime.Text = "Last refresh: 05/30/2019 10:22 AM";
            // 
            // btnRefreshSchools
            // 
            this.btnRefreshSchools.Location = new System.Drawing.Point(530, 11);
            this.btnRefreshSchools.Name = "btnRefreshSchools";
            this.btnRefreshSchools.Size = new System.Drawing.Size(110, 24);
            this.btnRefreshSchools.TabIndex = 3;
            this.btnRefreshSchools.Text = "Refresh Schools";
            this.btnRefreshSchools.Click += new System.EventHandler(this.btnRefreshSchools_Click);
            // 
            // btnRefreshStudents
            // 
            this.btnRefreshStudents.Location = new System.Drawing.Point(650, 11);
            this.btnRefreshStudents.Name = "btnRefreshStudents";
            this.btnRefreshStudents.Size = new System.Drawing.Size(110, 24);
            this.btnRefreshStudents.TabIndex = 3;
            this.btnRefreshStudents.Text = "Refresh Students";
            this.btnRefreshStudents.Click += new System.EventHandler(this.btnRefreshStudents_Click);
            // 
            // cmbSchools
            // 
            this.cmbSchools.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSchools.DataSource = this.schoolsBindingSource;
            this.cmbSchools.DropDownAnimationEnabled = false;
            this.cmbSchools.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmbSchools.Location = new System.Drawing.Point(140, 10);
            this.cmbSchools.Name = "cmbSchools";
            this.cmbSchools.Size = new System.Drawing.Size(379, 27);
            this.cmbSchools.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.cmbSchools.TabIndex = 1;
            this.cmbSchools.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbSchools_SelectedIndexChanged);
            // 
            // schoolsBindingSource
            // 
            this.schoolsBindingSource.DataMember = "Schools";
            this.schoolsBindingSource.DataSource = this.schoolsDataset;
            // 
            // schoolsDataset
            // 
            this.schoolsDataset.DataSetName = "SchoolsDataset";
            this.schoolsDataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblSchool
            // 
            this.lblSchool.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblSchool.Location = new System.Drawing.Point(22, 11);
            this.lblSchool.Name = "lblSchool";
            this.lblSchool.Size = new System.Drawing.Size(108, 25);
            this.lblSchool.TabIndex = 2;
            this.lblSchool.Text = "Select School:";
            // 
            // btnFullScreen
            // 
            this.btnFullScreen.Location = new System.Drawing.Point(1020, 8);
            this.btnFullScreen.Name = "btnFullScreen";
            this.btnFullScreen.Size = new System.Drawing.Size(110, 24);
            this.btnFullScreen.TabIndex = 4;
            this.btnFullScreen.Text = "Full Screen";
            this.btnFullScreen.Click += new System.EventHandler(this.btnFullScreen_Click);
            // 
            // pnlChildSearch
            // 
            this.pnlChildSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlChildSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlChildSearch.HeaderText = "Child Search";
            this.pnlChildSearch.Location = new System.Drawing.Point(0, 79);
            this.pnlChildSearch.Name = "pnlChildSearch";
            this.pnlChildSearch.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 910, 242);
            // 
            // pnlChildSearch.PanelContainer
            // 
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkLunchTimeColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkBColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkYColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkRColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkCommentsColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkTeacherFirstColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkHomeRoomColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkSpecialTimeColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkRecessColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGradeColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGenderColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkDOBColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkLanguageColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkTeacherLastColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkFirstNameColumnVisible);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.radGroupBox3);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.radGroupBox2);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.radGroupBox1);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.frpDentist);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGrade5th);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGrade4th);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGrade3rd);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGrade2nd);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGrade1st);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGradeK);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.chkGradeAll);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.lblFilterGrade);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.cmbStatus);
            this.pnlChildSearch.PanelContainer.Controls.Add(this.lblFilterStatus);
            this.pnlChildSearch.PanelContainer.Size = new System.Drawing.Size(1247, 325);
            this.pnlChildSearch.Size = new System.Drawing.Size(1249, 354);
            this.pnlChildSearch.TabIndex = 5;
            this.pnlChildSearch.Expanded += new System.EventHandler(this.pnlChildSearch_Expanded);
            this.pnlChildSearch.Collapsed += new System.EventHandler(this.pnlChildSearch_Collapsed);
            // 
            // chkLunchTimeColumnVisible
            // 
            this.chkLunchTimeColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLunchTimeColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLunchTimeColumnVisible.Location = new System.Drawing.Point(432, 286);
            this.chkLunchTimeColumnVisible.Name = "chkLunchTimeColumnVisible";
            this.chkLunchTimeColumnVisible.Size = new System.Drawing.Size(79, 27);
            this.chkLunchTimeColumnVisible.TabIndex = 22;
            this.chkLunchTimeColumnVisible.Text = "Lunch";
            this.chkLunchTimeColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkLunchTimeColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkLunchTimeColumnVisible_ToggleStateChanged);
            // 
            // chkBColumnVisible
            // 
            this.chkBColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBColumnVisible.Location = new System.Drawing.Point(1181, 286);
            this.chkBColumnVisible.Name = "chkBColumnVisible";
            this.chkBColumnVisible.Size = new System.Drawing.Size(37, 27);
            this.chkBColumnVisible.TabIndex = 27;
            this.chkBColumnVisible.Text = "B";
            this.chkBColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkBColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkBColumnVisible_ToggleStateChanged);
            // 
            // chkYColumnVisible
            // 
            this.chkYColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkYColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkYColumnVisible.Location = new System.Drawing.Point(1142, 286);
            this.chkYColumnVisible.Name = "chkYColumnVisible";
            this.chkYColumnVisible.Size = new System.Drawing.Size(37, 27);
            this.chkYColumnVisible.TabIndex = 26;
            this.chkYColumnVisible.Text = "Y";
            this.chkYColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkYColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkYColumnVisible_ToggleStateChanged);
            // 
            // chkGColumnVisible
            // 
            this.chkGColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGColumnVisible.Location = new System.Drawing.Point(1101, 286);
            this.chkGColumnVisible.Name = "chkGColumnVisible";
            this.chkGColumnVisible.Size = new System.Drawing.Size(39, 27);
            this.chkGColumnVisible.TabIndex = 25;
            this.chkGColumnVisible.Text = "G";
            this.chkGColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGColumnVisible_ToggleStateChanged);
            // 
            // chkRColumnVisible
            // 
            this.chkRColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRColumnVisible.Location = new System.Drawing.Point(1061, 286);
            this.chkRColumnVisible.Name = "chkRColumnVisible";
            this.chkRColumnVisible.Size = new System.Drawing.Size(38, 27);
            this.chkRColumnVisible.TabIndex = 24;
            this.chkRColumnVisible.Text = "R";
            this.chkRColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkRColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkRColumnVisible_ToggleStateChanged);
            // 
            // chkCommentsColumnVisible
            // 
            this.chkCommentsColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCommentsColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCommentsColumnVisible.Location = new System.Drawing.Point(982, 286);
            this.chkCommentsColumnVisible.Name = "chkCommentsColumnVisible";
            this.chkCommentsColumnVisible.Size = new System.Drawing.Size(77, 27);
            this.chkCommentsColumnVisible.TabIndex = 23;
            this.chkCommentsColumnVisible.Text = "Notes";
            this.chkCommentsColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkCommentsColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkCommentsColumnVisible_ToggleStateChanged);
            // 
            // chkTeacherFirstColumnVisible
            // 
            this.chkTeacherFirstColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTeacherFirstColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTeacherFirstColumnVisible.Location = new System.Drawing.Point(119, 286);
            this.chkTeacherFirstColumnVisible.Name = "chkTeacherFirstColumnVisible";
            this.chkTeacherFirstColumnVisible.Size = new System.Drawing.Size(122, 27);
            this.chkTeacherFirstColumnVisible.TabIndex = 16;
            this.chkTeacherFirstColumnVisible.Text = "TchFname";
            this.chkTeacherFirstColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkTeacherFirstColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkTeacherFirstColumnVisible_ToggleStateChanged);
            // 
            // chkHomeRoomColumnVisible
            // 
            this.chkHomeRoomColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHomeRoomColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHomeRoomColumnVisible.Location = new System.Drawing.Point(706, 286);
            this.chkHomeRoomColumnVisible.Name = "chkHomeRoomColumnVisible";
            this.chkHomeRoomColumnVisible.Size = new System.Drawing.Size(87, 27);
            this.chkHomeRoomColumnVisible.TabIndex = 21;
            this.chkHomeRoomColumnVisible.Text = "HmRm";
            this.chkHomeRoomColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkHomeRoomColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHomeRoomColumnVisible_ToggleStateChanged);
            // 
            // chkSpecialTimeColumnVisible
            // 
            this.chkSpecialTimeColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSpecialTimeColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSpecialTimeColumnVisible.Location = new System.Drawing.Point(795, 286);
            this.chkSpecialTimeColumnVisible.Name = "chkSpecialTimeColumnVisible";
            this.chkSpecialTimeColumnVisible.Size = new System.Drawing.Size(91, 27);
            this.chkSpecialTimeColumnVisible.TabIndex = 21;
            this.chkSpecialTimeColumnVisible.Text = "Special";
            this.chkSpecialTimeColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkSpecialTimeColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkSpecialTimeColumnVisible_ToggleStateChanged);
            // 
            // chkRecessColumnVisible
            // 
            this.chkRecessColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRecessColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRecessColumnVisible.Location = new System.Drawing.Point(888, 286);
            this.chkRecessColumnVisible.Name = "chkRecessColumnVisible";
            this.chkRecessColumnVisible.Size = new System.Drawing.Size(92, 27);
            this.chkRecessColumnVisible.TabIndex = 21;
            this.chkRecessColumnVisible.Text = "Recess";
            this.chkRecessColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkRecessColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkRecessColumnVisible_ToggleStateChanged);
            // 
            // chkGradeColumnVisible
            // 
            this.chkGradeColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGradeColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGradeColumnVisible.Location = new System.Drawing.Point(646, 286);
            this.chkGradeColumnVisible.Name = "chkGradeColumnVisible";
            this.chkGradeColumnVisible.Size = new System.Drawing.Size(58, 27);
            this.chkGradeColumnVisible.TabIndex = 21;
            this.chkGradeColumnVisible.Text = "Grd";
            this.chkGradeColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGradeColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade_ToggleStateChanged);
            // 
            // chkGenderColumnVisible
            // 
            this.chkGenderColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGenderColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGenderColumnVisible.Location = new System.Drawing.Point(582, 286);
            this.chkGenderColumnVisible.Name = "chkGenderColumnVisible";
            this.chkGenderColumnVisible.Size = new System.Drawing.Size(62, 27);
            this.chkGenderColumnVisible.TabIndex = 20;
            this.chkGenderColumnVisible.Text = "Gen";
            this.chkGenderColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGenderColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGenderColumnVisible_ToggleStateChanged);
            // 
            // chkDOBColumnVisible
            // 
            this.chkDOBColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDOBColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDOBColumnVisible.Location = new System.Drawing.Point(512, 286);
            this.chkDOBColumnVisible.Name = "chkDOBColumnVisible";
            this.chkDOBColumnVisible.Size = new System.Drawing.Size(68, 27);
            this.chkDOBColumnVisible.TabIndex = 19;
            this.chkDOBColumnVisible.Text = "DOB";
            this.chkDOBColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkDOBColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDOBColumnVisible_ToggleStateChanged);
            // 
            // chkLanguageColumnVisible
            // 
            this.chkLanguageColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLanguageColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLanguageColumnVisible.Location = new System.Drawing.Point(365, 286);
            this.chkLanguageColumnVisible.Name = "chkLanguageColumnVisible";
            this.chkLanguageColumnVisible.Size = new System.Drawing.Size(69, 27);
            this.chkLanguageColumnVisible.TabIndex = 18;
            this.chkLanguageColumnVisible.Text = "Lang";
            this.chkLanguageColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkLanguageColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkLanguageColumnVisible_ToggleStateChanged);
            // 
            // chkTeacherLastColumnVisible
            // 
            this.chkTeacherLastColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTeacherLastColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTeacherLastColumnVisible.Location = new System.Drawing.Point(243, 286);
            this.chkTeacherLastColumnVisible.Name = "chkTeacherLastColumnVisible";
            this.chkTeacherLastColumnVisible.Size = new System.Drawing.Size(120, 27);
            this.chkTeacherLastColumnVisible.TabIndex = 17;
            this.chkTeacherLastColumnVisible.Text = "TchLname";
            this.chkTeacherLastColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkTeacherLastColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkTeacherLastColumnVisible_ToggleStateChanged);
            // 
            // chkFirstNameColumnVisible
            // 
            this.chkFirstNameColumnVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFirstNameColumnVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFirstNameColumnVisible.Location = new System.Drawing.Point(27, 286);
            this.chkFirstNameColumnVisible.Name = "chkFirstNameColumnVisible";
            this.chkFirstNameColumnVisible.Size = new System.Drawing.Size(87, 27);
            this.chkFirstNameColumnVisible.TabIndex = 13;
            this.chkFirstNameColumnVisible.Text = "Fname";
            this.chkFirstNameColumnVisible.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkFirstNameColumnVisible.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkFirstNameColumnVisible_ToggleStateChanged);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(255)))));
            this.radGroupBox3.Controls.Add(this.txtAfterSchoolDDS);
            this.radGroupBox3.Controls.Add(this.txtAfterSchoolHyg);
            this.radGroupBox3.Controls.Add(this.txtAfterSchoolCustom);
            this.radGroupBox3.Controls.Add(this.chkAfterSchoolCustom);
            this.radGroupBox3.Controls.Add(this.chkAfterSchoolReferralConsult);
            this.radGroupBox3.Controls.Add(this.chkAfterSchoolDDS);
            this.radGroupBox3.Controls.Add(this.chkAfterSchoolHyg);
            this.radGroupBox3.Controls.Add(this.chkAfterSchool);
            this.radGroupBox3.HeaderText = "";
            this.radGroupBox3.Location = new System.Drawing.Point(939, 49);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(303, 231);
            this.radGroupBox3.TabIndex = 12;
            // 
            // txtAfterSchoolDDS
            // 
            this.txtAfterSchoolDDS.Location = new System.Drawing.Point(133, 74);
            this.txtAfterSchoolDDS.Name = "txtAfterSchoolDDS";
            this.txtAfterSchoolDDS.Size = new System.Drawing.Size(153, 20);
            this.txtAfterSchoolDDS.TabIndex = 27;
            this.txtAfterSchoolDDS.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtAfterSchoolDDS_TextChanging);
            // 
            // txtAfterSchoolHyg
            // 
            this.txtAfterSchoolHyg.Location = new System.Drawing.Point(133, 46);
            this.txtAfterSchoolHyg.Name = "txtAfterSchoolHyg";
            this.txtAfterSchoolHyg.Size = new System.Drawing.Size(153, 20);
            this.txtAfterSchoolHyg.TabIndex = 26;
            this.txtAfterSchoolHyg.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtAfterSchoolHyg_TextChanging);
            // 
            // txtAfterSchoolCustom
            // 
            this.txtAfterSchoolCustom.Location = new System.Drawing.Point(73, 128);
            this.txtAfterSchoolCustom.Name = "txtAfterSchoolCustom";
            this.txtAfterSchoolCustom.Size = new System.Drawing.Size(213, 20);
            this.txtAfterSchoolCustom.TabIndex = 24;
            this.txtAfterSchoolCustom.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtAfterSchoolCustom_TextChanging);
            // 
            // chkAfterSchoolCustom
            // 
            this.chkAfterSchoolCustom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAfterSchoolCustom.Location = new System.Drawing.Point(52, 131);
            this.chkAfterSchoolCustom.Name = "chkAfterSchoolCustom";
            this.chkAfterSchoolCustom.Size = new System.Drawing.Size(15, 15);
            this.chkAfterSchoolCustom.TabIndex = 19;
            this.chkAfterSchoolCustom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAfterSchoolCustom.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkAfterSchoolCustom_ToggleStateChanged);
            // 
            // chkAfterSchoolReferralConsult
            // 
            this.chkAfterSchoolReferralConsult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAfterSchoolReferralConsult.Location = new System.Drawing.Point(52, 97);
            this.chkAfterSchoolReferralConsult.Name = "chkAfterSchoolReferralConsult";
            this.chkAfterSchoolReferralConsult.Size = new System.Drawing.Size(172, 27);
            this.chkAfterSchoolReferralConsult.TabIndex = 15;
            this.chkAfterSchoolReferralConsult.Text = "Referral Consult";
            this.chkAfterSchoolReferralConsult.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAfterSchoolReferralConsult.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkAfterSchoolRefferalConsult_ToggleStateChanged);
            // 
            // chkAfterSchoolDDS
            // 
            this.chkAfterSchoolDDS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAfterSchoolDDS.Location = new System.Drawing.Point(52, 70);
            this.chkAfterSchoolDDS.Name = "chkAfterSchoolDDS";
            this.chkAfterSchoolDDS.Size = new System.Drawing.Size(67, 27);
            this.chkAfterSchoolDDS.TabIndex = 14;
            this.chkAfterSchoolDDS.Text = "DDS";
            this.chkAfterSchoolDDS.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAfterSchoolDDS.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkAfterSchoolDDS_ToggleStateChanged);
            // 
            // chkAfterSchoolHyg
            // 
            this.chkAfterSchoolHyg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAfterSchoolHyg.Location = new System.Drawing.Point(52, 43);
            this.chkAfterSchoolHyg.Name = "chkAfterSchoolHyg";
            this.chkAfterSchoolHyg.Size = new System.Drawing.Size(60, 27);
            this.chkAfterSchoolHyg.TabIndex = 13;
            this.chkAfterSchoolHyg.Text = "Hyg";
            this.chkAfterSchoolHyg.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAfterSchoolHyg.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkAfterSchoolHyg_ToggleStateChanged);
            // 
            // chkAfterSchool
            // 
            this.chkAfterSchool.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAfterSchool.Location = new System.Drawing.Point(35, 10);
            this.chkAfterSchool.Name = "chkAfterSchool";
            this.chkAfterSchool.Size = new System.Drawing.Size(135, 27);
            this.chkAfterSchool.TabIndex = 12;
            this.chkAfterSchool.Text = "After School";
            this.chkAfterSchool.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAfterSchool.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkAfterSchool_ToggleStateChanged);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(120)))));
            this.radGroupBox2.Controls.Add(this.chkComplexPatientWheelchair);
            this.radGroupBox2.Controls.Add(this.txtComplexPatientCustom);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientCustom);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientSpecialNeeds);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientGagReflex);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientLimitedOpening);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientAnesthetic);
            this.radGroupBox2.Controls.Add(this.chkComplexPatientBehavior);
            this.radGroupBox2.Controls.Add(this.chkComplexPatient);
            this.radGroupBox2.HeaderText = "";
            this.radGroupBox2.Location = new System.Drawing.Point(632, 49);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(303, 231);
            this.radGroupBox2.TabIndex = 12;
            // 
            // chkComplexPatientWheelchair
            // 
            this.chkComplexPatientWheelchair.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientWheelchair.Location = new System.Drawing.Point(53, 174);
            this.chkComplexPatientWheelchair.Name = "chkComplexPatientWheelchair";
            this.chkComplexPatientWheelchair.Size = new System.Drawing.Size(126, 27);
            this.chkComplexPatientWheelchair.TabIndex = 31;
            this.chkComplexPatientWheelchair.Text = "Wheelchair";
            this.chkComplexPatientWheelchair.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientWheelchair.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientWheelchair_ToggleStateChanged);
            // 
            // txtComplexPatientCustom
            // 
            this.txtComplexPatientCustom.Location = new System.Drawing.Point(73, 204);
            this.txtComplexPatientCustom.Name = "txtComplexPatientCustom";
            this.txtComplexPatientCustom.Size = new System.Drawing.Size(195, 20);
            this.txtComplexPatientCustom.TabIndex = 33;
            this.txtComplexPatientCustom.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtComplexPatientCustom_TextChanging);
            // 
            // chkComplexPatientCustom
            // 
            this.chkComplexPatientCustom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientCustom.Location = new System.Drawing.Point(52, 207);
            this.chkComplexPatientCustom.Name = "chkComplexPatientCustom";
            this.chkComplexPatientCustom.Size = new System.Drawing.Size(15, 15);
            this.chkComplexPatientCustom.TabIndex = 32;
            this.chkComplexPatientCustom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientCustom.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientCustom_ToggleStateChanged);
            // 
            // chkComplexPatientSpecialNeeds
            // 
            this.chkComplexPatientSpecialNeeds.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientSpecialNeeds.Location = new System.Drawing.Point(52, 147);
            this.chkComplexPatientSpecialNeeds.Name = "chkComplexPatientSpecialNeeds";
            this.chkComplexPatientSpecialNeeds.Size = new System.Drawing.Size(156, 27);
            this.chkComplexPatientSpecialNeeds.TabIndex = 30;
            this.chkComplexPatientSpecialNeeds.Text = "Special Needs";
            this.chkComplexPatientSpecialNeeds.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientSpecialNeeds.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientSpecialNeeds_ToggleStateChanged);
            // 
            // chkComplexPatientGagReflex
            // 
            this.chkComplexPatientGagReflex.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientGagReflex.Location = new System.Drawing.Point(52, 120);
            this.chkComplexPatientGagReflex.Name = "chkComplexPatientGagReflex";
            this.chkComplexPatientGagReflex.Size = new System.Drawing.Size(126, 27);
            this.chkComplexPatientGagReflex.TabIndex = 29;
            this.chkComplexPatientGagReflex.Text = "Gag Reflex";
            this.chkComplexPatientGagReflex.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientGagReflex.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientGagReflex_ToggleStateChanged);
            // 
            // chkComplexPatientLimitedOpening
            // 
            this.chkComplexPatientLimitedOpening.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientLimitedOpening.Location = new System.Drawing.Point(52, 93);
            this.chkComplexPatientLimitedOpening.Name = "chkComplexPatientLimitedOpening";
            this.chkComplexPatientLimitedOpening.Size = new System.Drawing.Size(173, 27);
            this.chkComplexPatientLimitedOpening.TabIndex = 28;
            this.chkComplexPatientLimitedOpening.Text = "Limited Opening";
            this.chkComplexPatientLimitedOpening.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientLimitedOpening.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientLimitedOpening_ToggleStateChanged);
            // 
            // chkComplexPatientAnesthetic
            // 
            this.chkComplexPatientAnesthetic.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientAnesthetic.Location = new System.Drawing.Point(52, 66);
            this.chkComplexPatientAnesthetic.Name = "chkComplexPatientAnesthetic";
            this.chkComplexPatientAnesthetic.Size = new System.Drawing.Size(119, 27);
            this.chkComplexPatientAnesthetic.TabIndex = 27;
            this.chkComplexPatientAnesthetic.Text = "Anesthetic";
            this.chkComplexPatientAnesthetic.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientAnesthetic.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientAnesthetic_ToggleStateChanged);
            // 
            // chkComplexPatientBehavior
            // 
            this.chkComplexPatientBehavior.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatientBehavior.Location = new System.Drawing.Point(52, 39);
            this.chkComplexPatientBehavior.Name = "chkComplexPatientBehavior";
            this.chkComplexPatientBehavior.Size = new System.Drawing.Size(105, 27);
            this.chkComplexPatientBehavior.TabIndex = 26;
            this.chkComplexPatientBehavior.Text = "Behavior";
            this.chkComplexPatientBehavior.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatientBehavior.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatientBehavior_ToggleStateChanged);
            // 
            // chkComplexPatient
            // 
            this.chkComplexPatient.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkComplexPatient.Location = new System.Drawing.Point(35, 6);
            this.chkComplexPatient.Name = "chkComplexPatient";
            this.chkComplexPatient.Size = new System.Drawing.Size(174, 27);
            this.chkComplexPatient.TabIndex = 25;
            this.chkComplexPatient.Text = "Complex Patient";
            this.chkComplexPatient.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkComplexPatient.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkComplexPatient_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(255)))), ((int)(((byte)(120)))));
            this.radGroupBox1.Controls.Add(this.txtHygienistSealant);
            this.radGroupBox1.Controls.Add(this.txtHygienistCustom);
            this.radGroupBox1.Controls.Add(this.chkHygienistCustom);
            this.radGroupBox1.Controls.Add(this.chkHygienistSealant);
            this.radGroupBox1.Controls.Add(this.chkHygienistPX2);
            this.radGroupBox1.Controls.Add(this.chkHygienistPX1);
            this.radGroupBox1.Controls.Add(this.chkHygienistHygieneFirst);
            this.radGroupBox1.Controls.Add(this.chkHygienistFluoride);
            this.radGroupBox1.Controls.Add(this.chkHygienist);
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(326, 49);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(303, 231);
            this.radGroupBox1.TabIndex = 12;
            // 
            // txtHygienistSealant
            // 
            this.txtHygienistSealant.Location = new System.Drawing.Point(151, 154);
            this.txtHygienistSealant.Name = "txtHygienistSealant";
            this.txtHygienistSealant.Size = new System.Drawing.Size(117, 20);
            this.txtHygienistSealant.TabIndex = 25;
            this.txtHygienistSealant.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtHygienistSealant_TextChanging);
            // 
            // txtHygienistCustom
            // 
            this.txtHygienistCustom.Location = new System.Drawing.Point(73, 179);
            this.txtHygienistCustom.Name = "txtHygienistCustom";
            this.txtHygienistCustom.Size = new System.Drawing.Size(195, 20);
            this.txtHygienistCustom.TabIndex = 24;
            this.txtHygienistCustom.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtHygienistCustom_TextChanging);
            // 
            // chkHygienistCustom
            // 
            this.chkHygienistCustom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistCustom.Location = new System.Drawing.Point(52, 182);
            this.chkHygienistCustom.Name = "chkHygienistCustom";
            this.chkHygienistCustom.Size = new System.Drawing.Size(15, 15);
            this.chkHygienistCustom.TabIndex = 19;
            this.chkHygienistCustom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistCustom.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistCustom_ToggleStateChanged);
            // 
            // chkHygienistSealant
            // 
            this.chkHygienistSealant.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistSealant.Location = new System.Drawing.Point(52, 151);
            this.chkHygienistSealant.Name = "chkHygienistSealant";
            this.chkHygienistSealant.Size = new System.Drawing.Size(93, 27);
            this.chkHygienistSealant.TabIndex = 17;
            this.chkHygienistSealant.Text = "Sealant";
            this.chkHygienistSealant.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistSealant.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistSealant_ToggleStateChanged);
            // 
            // chkHygienistPX2
            // 
            this.chkHygienistPX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistPX2.Location = new System.Drawing.Point(52, 124);
            this.chkHygienistPX2.Name = "chkHygienistPX2";
            this.chkHygienistPX2.Size = new System.Drawing.Size(62, 27);
            this.chkHygienistPX2.TabIndex = 16;
            this.chkHygienistPX2.Text = "PX2";
            this.chkHygienistPX2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistPX2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistPX2_ToggleStateChanged);
            // 
            // chkHygienistPX1
            // 
            this.chkHygienistPX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistPX1.Location = new System.Drawing.Point(52, 97);
            this.chkHygienistPX1.Name = "chkHygienistPX1";
            this.chkHygienistPX1.Size = new System.Drawing.Size(62, 27);
            this.chkHygienistPX1.TabIndex = 15;
            this.chkHygienistPX1.Text = "PX1";
            this.chkHygienistPX1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistPX1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistPX1_ToggleStateChanged);
            // 
            // chkHygienistHygieneFirst
            // 
            this.chkHygienistHygieneFirst.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistHygieneFirst.Location = new System.Drawing.Point(52, 70);
            this.chkHygienistHygieneFirst.Name = "chkHygienistHygieneFirst";
            this.chkHygienistHygieneFirst.Size = new System.Drawing.Size(144, 27);
            this.chkHygienistHygieneFirst.TabIndex = 14;
            this.chkHygienistHygieneFirst.Text = "Hygiene First";
            this.chkHygienistHygieneFirst.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistHygieneFirst.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistHygieneFirst_ToggleStateChanged);
            // 
            // chkHygienistFluoride
            // 
            this.chkHygienistFluoride.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienistFluoride.Location = new System.Drawing.Point(52, 43);
            this.chkHygienistFluoride.Name = "chkHygienistFluoride";
            this.chkHygienistFluoride.Size = new System.Drawing.Size(98, 27);
            this.chkHygienistFluoride.TabIndex = 13;
            this.chkHygienistFluoride.Text = "Fluoride";
            this.chkHygienistFluoride.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienistFluoride.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienistFluoride_ToggleStateChanged);
            // 
            // chkHygienist
            // 
            this.chkHygienist.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHygienist.Location = new System.Drawing.Point(37, 10);
            this.chkHygienist.Name = "chkHygienist";
            this.chkHygienist.Size = new System.Drawing.Size(108, 27);
            this.chkHygienist.TabIndex = 12;
            this.chkHygienist.Text = "Hygienist";
            this.chkHygienist.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkHygienist.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkHygienist_ToggleStateChanged);
            // 
            // frpDentist
            // 
            this.frpDentist.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frpDentist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.frpDentist.Controls.Add(this.txtDentistCustom);
            this.frpDentist.Controls.Add(this.label1);
            this.frpDentist.Controls.Add(this.chkDentistLRAnes);
            this.frpDentist.Controls.Add(this.chkDentistCustom);
            this.frpDentist.Controls.Add(this.chkDentistLLAnes);
            this.frpDentist.Controls.Add(this.chkDentistIntake);
            this.frpDentist.Controls.Add(this.chkDentistULAnes);
            this.frpDentist.Controls.Add(this.chkDentistLR);
            this.frpDentist.Controls.Add(this.chkDentistURAnes);
            this.frpDentist.Controls.Add(this.chkDentistLL);
            this.frpDentist.Controls.Add(this.chkDentistUL);
            this.frpDentist.Controls.Add(this.chkDentistUR);
            this.frpDentist.Controls.Add(this.chkDentist);
            this.frpDentist.HeaderText = "";
            this.frpDentist.Location = new System.Drawing.Point(22, 49);
            this.frpDentist.Name = "frpDentist";
            this.frpDentist.Size = new System.Drawing.Size(303, 231);
            this.frpDentist.TabIndex = 11;
            // 
            // txtDentistCustom
            // 
            this.txtDentistCustom.Location = new System.Drawing.Point(53, 178);
            this.txtDentistCustom.Name = "txtDentistCustom";
            this.txtDentistCustom.Size = new System.Drawing.Size(195, 20);
            this.txtDentistCustom.TabIndex = 11;
            this.txtDentistCustom.TextChanging += new Telerik.WinControls.TextChangingEventHandler(this.txtDentistCustom_TextChanging);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(143, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Anes";
            // 
            // chkDentistLRAnes
            // 
            this.chkDentistLRAnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistLRAnes.Location = new System.Drawing.Point(146, 123);
            this.chkDentistLRAnes.Name = "chkDentistLRAnes";
            this.chkDentistLRAnes.Size = new System.Drawing.Size(15, 15);
            this.chkDentistLRAnes.TabIndex = 9;
            this.chkDentistLRAnes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistLRAnes.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistLRAnes_ToggleStateChanged);
            // 
            // chkDentistCustom
            // 
            this.chkDentistCustom.EnableCodedUITests = true;
            this.chkDentistCustom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistCustom.Location = new System.Drawing.Point(32, 186);
            this.chkDentistCustom.Name = "chkDentistCustom";
            this.chkDentistCustom.Size = new System.Drawing.Size(15, 15);
            this.chkDentistCustom.TabIndex = 7;
            this.chkDentistCustom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistCustom.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistCustom_ToggleStateChanged);
            // 
            // chkDentistLLAnes
            // 
            this.chkDentistLLAnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistLLAnes.Location = new System.Drawing.Point(146, 96);
            this.chkDentistLLAnes.Name = "chkDentistLLAnes";
            this.chkDentistLLAnes.Size = new System.Drawing.Size(15, 15);
            this.chkDentistLLAnes.TabIndex = 8;
            this.chkDentistLLAnes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistLLAnes.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistLLAnes_ToggleStateChanged);
            // 
            // chkDentistIntake
            // 
            this.chkDentistIntake.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistIntake.Location = new System.Drawing.Point(32, 150);
            this.chkDentistIntake.Name = "chkDentistIntake";
            this.chkDentistIntake.Size = new System.Drawing.Size(79, 27);
            this.chkDentistIntake.TabIndex = 6;
            this.chkDentistIntake.Text = "Intake";
            this.chkDentistIntake.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistIntake.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistIntake_ToggleStateChanged);
            this.chkDentistIntake.Click += new System.EventHandler(this.chkDentistIntake_Click);
            // 
            // chkDentistULAnes
            // 
            this.chkDentistULAnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistULAnes.Location = new System.Drawing.Point(146, 70);
            this.chkDentistULAnes.Name = "chkDentistULAnes";
            this.chkDentistULAnes.Size = new System.Drawing.Size(15, 15);
            this.chkDentistULAnes.TabIndex = 7;
            this.chkDentistULAnes.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistULAnes.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistULAnes_ToggleStateChanged);
            // 
            // chkDentistLR
            // 
            this.chkDentistLR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistLR.Location = new System.Drawing.Point(32, 123);
            this.chkDentistLR.Name = "chkDentistLR";
            this.chkDentistLR.Size = new System.Drawing.Size(49, 27);
            this.chkDentistLR.TabIndex = 5;
            this.chkDentistLR.Text = "LR";
            this.chkDentistLR.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistLR.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistLR_ToggleStateChanged);
            // 
            // chkDentistURAnes
            // 
            this.chkDentistURAnes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistURAnes.Location = new System.Drawing.Point(146, 42);
            this.chkDentistURAnes.Name = "chkDentistURAnes";
            this.chkDentistURAnes.Size = new System.Drawing.Size(15, 15);
            this.chkDentistURAnes.TabIndex = 6;
            this.chkDentistURAnes.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDentistURAnes.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistURAnes_ToggleStateChanged);
            // 
            // chkDentistLL
            // 
            this.chkDentistLL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistLL.Location = new System.Drawing.Point(32, 96);
            this.chkDentistLL.Name = "chkDentistLL";
            this.chkDentistLL.Size = new System.Drawing.Size(46, 27);
            this.chkDentistLL.TabIndex = 4;
            this.chkDentistLL.Text = "LL";
            this.chkDentistLL.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistLL.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistLL_ToggleStateChanged);
            // 
            // chkDentistUL
            // 
            this.chkDentistUL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistUL.Location = new System.Drawing.Point(32, 69);
            this.chkDentistUL.Name = "chkDentistUL";
            this.chkDentistUL.Size = new System.Drawing.Size(49, 27);
            this.chkDentistUL.TabIndex = 3;
            this.chkDentistUL.Text = "UL";
            this.chkDentistUL.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistUL.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistUL_ToggleStateChanged);
            // 
            // chkDentistUR
            // 
            this.chkDentistUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentistUR.Location = new System.Drawing.Point(32, 42);
            this.chkDentistUR.Name = "chkDentistUR";
            this.chkDentistUR.Size = new System.Drawing.Size(53, 27);
            this.chkDentistUR.TabIndex = 2;
            this.chkDentistUR.Text = "UR";
            this.chkDentistUR.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentistUR.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentistUR_ToggleStateChanged);
            // 
            // chkDentist
            // 
            this.chkDentist.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDentist.Location = new System.Drawing.Point(15, 9);
            this.chkDentist.Name = "chkDentist";
            this.chkDentist.Size = new System.Drawing.Size(87, 27);
            this.chkDentist.TabIndex = 1;
            this.chkDentist.Text = "Dentist";
            this.chkDentist.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkDentist.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkDentist_ToggleStateChanged);
            // 
            // chkGrade5th
            // 
            this.chkGrade5th.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrade5th.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGrade5th.Location = new System.Drawing.Point(883, 16);
            this.chkGrade5th.Name = "chkGrade5th";
            this.chkGrade5th.Size = new System.Drawing.Size(52, 27);
            this.chkGrade5th.TabIndex = 10;
            this.chkGrade5th.Text = "5th";
            this.chkGrade5th.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGrade5th.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade5th_ToggleStateChanged);
            // 
            // chkGrade4th
            // 
            this.chkGrade4th.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrade4th.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGrade4th.Location = new System.Drawing.Point(821, 16);
            this.chkGrade4th.Name = "chkGrade4th";
            this.chkGrade4th.Size = new System.Drawing.Size(52, 27);
            this.chkGrade4th.TabIndex = 9;
            this.chkGrade4th.Text = "4th";
            this.chkGrade4th.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGrade4th.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade4th_ToggleStateChanged);
            // 
            // chkGrade3rd
            // 
            this.chkGrade3rd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrade3rd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGrade3rd.Location = new System.Drawing.Point(758, 16);
            this.chkGrade3rd.Name = "chkGrade3rd";
            this.chkGrade3rd.Size = new System.Drawing.Size(53, 27);
            this.chkGrade3rd.TabIndex = 8;
            this.chkGrade3rd.Text = "3rd";
            this.chkGrade3rd.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGrade3rd.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade3rd_ToggleStateChanged);
            // 
            // chkGrade2nd
            // 
            this.chkGrade2nd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrade2nd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGrade2nd.Location = new System.Drawing.Point(690, 16);
            this.chkGrade2nd.Name = "chkGrade2nd";
            this.chkGrade2nd.Size = new System.Drawing.Size(58, 27);
            this.chkGrade2nd.TabIndex = 7;
            this.chkGrade2nd.Text = "2nd";
            this.chkGrade2nd.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGrade2nd.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade2nd_ToggleStateChanged);
            // 
            // chkGrade1st
            // 
            this.chkGrade1st.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrade1st.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGrade1st.Location = new System.Drawing.Point(629, 16);
            this.chkGrade1st.Name = "chkGrade1st";
            this.chkGrade1st.Size = new System.Drawing.Size(51, 27);
            this.chkGrade1st.TabIndex = 6;
            this.chkGrade1st.Text = "1st";
            this.chkGrade1st.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGrade1st.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGrade1st_ToggleStateChanged);
            // 
            // chkGradeK
            // 
            this.chkGradeK.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGradeK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGradeK.Location = new System.Drawing.Point(582, 16);
            this.chkGradeK.Name = "chkGradeK";
            this.chkGradeK.Size = new System.Drawing.Size(37, 27);
            this.chkGradeK.TabIndex = 5;
            this.chkGradeK.Text = "K";
            this.chkGradeK.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGradeK.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGradeK_ToggleStateChanged);
            // 
            // chkGradeAll
            // 
            this.chkGradeAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGradeAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGradeAll.Location = new System.Drawing.Point(526, 16);
            this.chkGradeAll.Name = "chkGradeAll";
            this.chkGradeAll.Size = new System.Drawing.Size(46, 27);
            this.chkGradeAll.TabIndex = 0;
            this.chkGradeAll.Text = "All";
            this.chkGradeAll.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chkGradeAll.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGradeAll_ToggleStateChanged);
            // 
            // lblFilterGrade
            // 
            this.lblFilterGrade.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblFilterGrade.Location = new System.Drawing.Point(401, 17);
            this.lblFilterGrade.Name = "lblFilterGrade";
            this.lblFilterGrade.Size = new System.Drawing.Size(118, 25);
            this.lblFilterGrade.TabIndex = 4;
            this.lblFilterGrade.Text = "Filter by Grade:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownAnimationEnabled = false;
            this.cmbStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.cmbStatus.Font = new System.Drawing.Font("Segoe UI", 12F);
            radListDataItem1.Text = "Tx Incomplete";
            radListDataItem2.Text = "TX Complete";
            radListDataItem3.Text = "All";
            this.cmbStatus.Items.Add(radListDataItem1);
            this.cmbStatus.Items.Add(radListDataItem2);
            this.cmbStatus.Items.Add(radListDataItem3);
            this.cmbStatus.Location = new System.Drawing.Point(148, 16);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(246, 27);
            this.cmbStatus.TabIndex = 1;
            this.cmbStatus.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // lblFilterStatus
            // 
            this.lblFilterStatus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblFilterStatus.Location = new System.Drawing.Point(22, 17);
            this.lblFilterStatus.Name = "lblFilterStatus";
            this.lblFilterStatus.Size = new System.Drawing.Size(119, 25);
            this.lblFilterStatus.TabIndex = 2;
            this.lblFilterStatus.Text = "Filter by Status:";
            // 
            // pnlStudentPickList
            // 
            this.pnlStudentPickList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStudentPickList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlStudentPickList.HeaderText = "Student Pick List";
            this.pnlStudentPickList.Location = new System.Drawing.Point(0, 433);
            this.pnlStudentPickList.Name = "pnlStudentPickList";
            this.pnlStudentPickList.OwnerBoundsCache = new System.Drawing.Rectangle(0, 321, 931, 369);
            // 
            // pnlStudentPickList.PanelContainer
            // 
            this.pnlStudentPickList.PanelContainer.Controls.Add(this.panel2);
            this.pnlStudentPickList.PanelContainer.Controls.Add(this.panel1);
            this.pnlStudentPickList.PanelContainer.Size = new System.Drawing.Size(1247, 228);
            this.pnlStudentPickList.Size = new System.Drawing.Size(1249, 257);
            this.pnlStudentPickList.TabIndex = 6;
            this.pnlStudentPickList.Expanded += new System.EventHandler(this.pnlStudentPickList_Expanded);
            this.pnlStudentPickList.Collapsed += new System.EventHandler(this.pnlStudentPickList_Collapsed);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1247, 183);
            this.panel2.TabIndex = 2;
            // 
            // radGridView1
            // 
            this.radGridView1.AutoSizeRows = true;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.EnableCodedUITests = true;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewComboBoxColumn1.EnableExpressionEditor = true;
            gridViewComboBoxColumn1.FieldName = "Status";
            gridViewComboBoxColumn1.HeaderText = "Status";
            gridViewComboBoxColumn1.Name = "Status";
            gridViewComboBoxColumn1.Width = 657;
            gridViewTextBoxColumn1.FieldName = "StudentID";
            gridViewTextBoxColumn1.HeaderText = "StudentID";
            gridViewTextBoxColumn1.IsAutoGenerated = true;
            gridViewTextBoxColumn1.Name = "StudentID";
            gridViewTextBoxColumn1.Width = 27;
            gridViewTextBoxColumn2.FieldName = "StudentLastName";
            gridViewTextBoxColumn2.HeaderText = "StudentLastName";
            gridViewTextBoxColumn2.IsAutoGenerated = true;
            gridViewTextBoxColumn2.Name = "StudentLastName";
            gridViewTextBoxColumn2.Width = 27;
            gridViewTextBoxColumn3.FieldName = "StudentFirstName";
            gridViewTextBoxColumn3.HeaderText = "StudentFirstName";
            gridViewTextBoxColumn3.IsAutoGenerated = true;
            gridViewTextBoxColumn3.Name = "StudentFirstName";
            gridViewTextBoxColumn3.Width = 27;
            gridViewDateTimeColumn1.FieldName = "DOB";
            gridViewDateTimeColumn1.HeaderText = "DOB";
            gridViewDateTimeColumn1.IsAutoGenerated = true;
            gridViewDateTimeColumn1.Name = "DOB";
            gridViewDateTimeColumn1.Width = 27;
            gridViewTextBoxColumn4.FieldName = "HomeRoom";
            gridViewTextBoxColumn4.HeaderText = "HomeRoom";
            gridViewTextBoxColumn4.IsAutoGenerated = true;
            gridViewTextBoxColumn4.Name = "HomeRoom";
            gridViewTextBoxColumn4.Width = 27;
            gridViewTextBoxColumn5.AllowResize = false;
            gridViewTextBoxColumn5.FieldName = "Comments";
            gridViewTextBoxColumn5.HeaderText = "Comments";
            gridViewTextBoxColumn5.IsAutoGenerated = true;
            gridViewTextBoxColumn5.Multiline = true;
            gridViewTextBoxColumn5.Name = "Comments";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 27;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn6.FieldName = "Language";
            gridViewTextBoxColumn6.HeaderText = "Language";
            gridViewTextBoxColumn6.IsAutoGenerated = true;
            gridViewTextBoxColumn6.Name = "Language";
            gridViewTextBoxColumn6.Width = 27;
            gridViewTextBoxColumn7.FieldName = "LunchTime";
            gridViewTextBoxColumn7.HeaderText = "LunchTime";
            gridViewTextBoxColumn7.IsAutoGenerated = true;
            gridViewTextBoxColumn7.Name = "LunchTime";
            gridViewTextBoxColumn7.Width = 27;
            gridViewTextBoxColumn8.FieldName = "SpecialTime";
            gridViewTextBoxColumn8.HeaderText = "SpecialTime";
            gridViewTextBoxColumn8.IsAutoGenerated = true;
            gridViewTextBoxColumn8.Name = "SpecialTime";
            gridViewTextBoxColumn8.Width = 27;
            gridViewTextBoxColumn9.FieldName = "RecessTime";
            gridViewTextBoxColumn9.HeaderText = "RecessTime";
            gridViewTextBoxColumn9.IsAutoGenerated = true;
            gridViewTextBoxColumn9.Name = "RecessTime";
            gridViewTextBoxColumn9.Width = 27;
            gridViewTextBoxColumn10.FieldName = "Grade";
            gridViewTextBoxColumn10.HeaderText = "Grade";
            gridViewTextBoxColumn10.IsAutoGenerated = true;
            gridViewTextBoxColumn10.Name = "Grade";
            gridViewTextBoxColumn10.Width = 27;
            gridViewTextBoxColumn11.FieldName = "TeacherLastName";
            gridViewTextBoxColumn11.HeaderText = "TeacherLastName";
            gridViewTextBoxColumn11.IsAutoGenerated = true;
            gridViewTextBoxColumn11.Name = "TeacherLastName";
            gridViewTextBoxColumn11.Width = 27;
            gridViewTextBoxColumn12.FieldName = "Gender";
            gridViewTextBoxColumn12.HeaderText = "Gender";
            gridViewTextBoxColumn12.IsAutoGenerated = true;
            gridViewTextBoxColumn12.Name = "Gender";
            gridViewTextBoxColumn12.Width = 27;
            gridViewTextBoxColumn13.FieldName = "TeacherFirstName";
            gridViewTextBoxColumn13.HeaderText = "TeacherFirstName";
            gridViewTextBoxColumn13.IsAutoGenerated = true;
            gridViewTextBoxColumn13.Name = "TeacherFirstName";
            gridViewTextBoxColumn13.Width = 27;
            gridViewTextBoxColumn14.AllowResize = false;
            gridViewTextBoxColumn14.FieldName = "R";
            gridViewTextBoxColumn14.HeaderText = "R";
            gridViewTextBoxColumn14.IsAutoGenerated = true;
            gridViewTextBoxColumn14.Multiline = true;
            gridViewTextBoxColumn14.Name = "R";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.Width = 27;
            gridViewTextBoxColumn14.WrapText = true;
            gridViewTextBoxColumn15.AllowResize = false;
            gridViewTextBoxColumn15.FieldName = "G";
            gridViewTextBoxColumn15.HeaderText = "G";
            gridViewTextBoxColumn15.IsAutoGenerated = true;
            gridViewTextBoxColumn15.Multiline = true;
            gridViewTextBoxColumn15.Name = "G";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 27;
            gridViewTextBoxColumn15.WrapText = true;
            gridViewTextBoxColumn16.AllowResize = false;
            gridViewTextBoxColumn16.FieldName = "Y";
            gridViewTextBoxColumn16.HeaderText = "Y";
            gridViewTextBoxColumn16.IsAutoGenerated = true;
            gridViewTextBoxColumn16.Multiline = true;
            gridViewTextBoxColumn16.Name = "Y";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 27;
            gridViewTextBoxColumn16.WrapText = true;
            gridViewTextBoxColumn17.AllowResize = false;
            gridViewTextBoxColumn17.FieldName = "B";
            gridViewTextBoxColumn17.HeaderText = "B";
            gridViewTextBoxColumn17.IsAutoGenerated = true;
            gridViewTextBoxColumn17.Multiline = true;
            gridViewTextBoxColumn17.Name = "B";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 27;
            gridViewTextBoxColumn17.WrapText = true;
            gridViewTextBoxColumn18.FieldName = "SchoolID";
            gridViewTextBoxColumn18.HeaderText = "SchoolID";
            gridViewTextBoxColumn18.IsAutoGenerated = true;
            gridViewTextBoxColumn18.Name = "SchoolID";
            gridViewTextBoxColumn18.Width = 27;
            gridViewDateTimeColumn2.FieldName = "ActiveDate";
            gridViewDateTimeColumn2.HeaderText = "ActiveDate";
            gridViewDateTimeColumn2.IsAutoGenerated = true;
            gridViewDateTimeColumn2.Name = "ActiveDate";
            gridViewDateTimeColumn2.Width = 27;
            gridViewCheckBoxColumn1.FieldName = "treatment_accepted";
            gridViewCheckBoxColumn1.HeaderText = "treatment_accepted";
            gridViewCheckBoxColumn1.IsAutoGenerated = true;
            gridViewCheckBoxColumn1.Name = "treatment_accepted";
            gridViewCheckBoxColumn1.Width = 27;
            gridViewDateTimeColumn3.FieldName = "completion_date";
            gridViewDateTimeColumn3.HeaderText = "completion_date";
            gridViewDateTimeColumn3.IsAutoGenerated = true;
            gridViewDateTimeColumn3.Name = "completion_date";
            gridViewDateTimeColumn3.Width = 24;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewDateTimeColumn2,
            gridViewCheckBoxColumn1,
            gridViewDateTimeColumn3});
            this.radGridView1.MasterTemplate.DataSource = this.studentTableBindingSource;
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(1247, 183);
            this.radGridView1.TabIndex = 0;
            this.radGridView1.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.radGridView1_RowFormatting);
            this.radGridView1.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGridView1_CellFormatting);
            this.radGridView1.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView1_CellEditorInitialized);
            this.radGridView1.ValueChanged += new System.EventHandler(this.radGridView1_ValueChanged);
            this.radGridView1.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView1_CellValueChanged);
            this.radGridView1.DoubleClick += new System.EventHandler(this.radGridView1_DoubleClick);
            this.radGridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.radGridView1_MouseMove);
            // 
            // studentTableBindingSource
            // 
            this.studentTableBindingSource.DataMember = "StudentTable";
            this.studentTableBindingSource.DataSource = this.studentDataSetBindingSource;
            // 
            // studentDataSetBindingSource
            // 
            this.studentDataSetBindingSource.DataSource = this.studentDataSet;
            this.studentDataSetBindingSource.Position = 0;
            // 
            // studentDataSet
            // 
            this.studentDataSet.DataSetName = "StudentDataSet";
            this.studentDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblStudentCount);
            this.panel1.Controls.Add(this.btnClearSort);
            this.panel1.Controls.Add(this.btnFullScreen);
            this.panel1.Controls.Add(this.btnSort2);
            this.panel1.Controls.Add(this.ddlSort2);
            this.panel1.Controls.Add(this.btnSort3);
            this.panel1.Controls.Add(this.ddlSort3);
            this.panel1.Controls.Add(this.btnSort1);
            this.panel1.Controls.Add(this.ddlSort1);
            this.panel1.Controls.Add(this.lblSort);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1247, 45);
            this.panel1.TabIndex = 1;
            // 
            // lblStudentCount
            // 
            this.lblStudentCount.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentCount.Location = new System.Drawing.Point(11, 8);
            this.lblStudentCount.Name = "lblStudentCount";
            this.lblStudentCount.Size = new System.Drawing.Size(193, 25);
            this.lblStudentCount.TabIndex = 13;
            this.lblStudentCount.Text = "Student Pick List - (255)";
            // 
            // btnClearSort
            // 
            this.btnClearSort.Location = new System.Drawing.Point(880, 8);
            this.btnClearSort.Name = "btnClearSort";
            this.btnClearSort.Size = new System.Drawing.Size(110, 24);
            this.btnClearSort.TabIndex = 12;
            this.btnClearSort.Text = "Clear Sort";
            this.btnClearSort.Click += new System.EventHandler(this.btnClearSort_Click);
            // 
            // btnSort2
            // 
            this.btnSort2.BackColor = System.Drawing.Color.Transparent;
            this.btnSort2.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnSort2.Image = global::ToothPick.Properties.Resources.AtoZ;
            this.btnSort2.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSort2.Location = new System.Drawing.Point(624, 6);
            this.btnSort2.Name = "btnSort2";
            this.btnSort2.Size = new System.Drawing.Size(28, 28);
            this.btnSort2.TabIndex = 11;
            this.btnSort2.Text = "radToggleButton1";
            this.btnSort2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.btnSort2_ToggleStateChanged);
            // 
            // ddlSort2
            // 
            this.ddlSort2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ddlSort2.DropDownAnimationEnabled = false;
            this.ddlSort2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSort2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem4.Text = "Fname";
            radListDataItem5.Text = "Lname";
            radListDataItem6.Text = "TchFname";
            radListDataItem7.Text = "TchLname";
            radListDataItem8.Text = "Lang";
            radListDataItem9.Text = "DOB";
            radListDataItem10.Text = "Gen";
            radListDataItem11.Text = "Grd";
            radListDataItem12.Text = "HmRm";
            radListDataItem13.Text = "Special";
            radListDataItem14.Text = "Recess";
            radListDataItem15.Text = "Notes";
            radListDataItem16.Text = "R";
            radListDataItem17.Text = "G";
            radListDataItem18.Text = "B";
            radListDataItem19.Text = "Lunch";
            this.ddlSort2.Items.Add(radListDataItem4);
            this.ddlSort2.Items.Add(radListDataItem5);
            this.ddlSort2.Items.Add(radListDataItem6);
            this.ddlSort2.Items.Add(radListDataItem7);
            this.ddlSort2.Items.Add(radListDataItem8);
            this.ddlSort2.Items.Add(radListDataItem9);
            this.ddlSort2.Items.Add(radListDataItem10);
            this.ddlSort2.Items.Add(radListDataItem11);
            this.ddlSort2.Items.Add(radListDataItem12);
            this.ddlSort2.Items.Add(radListDataItem13);
            this.ddlSort2.Items.Add(radListDataItem14);
            this.ddlSort2.Items.Add(radListDataItem15);
            this.ddlSort2.Items.Add(radListDataItem16);
            this.ddlSort2.Items.Add(radListDataItem17);
            this.ddlSort2.Items.Add(radListDataItem18);
            this.ddlSort2.Items.Add(radListDataItem19);
            this.ddlSort2.Location = new System.Drawing.Point(465, 5);
            this.ddlSort2.Name = "ddlSort2";
            this.ddlSort2.Size = new System.Drawing.Size(143, 31);
            this.ddlSort2.TabIndex = 10;
            this.ddlSort2.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.ddlSort2_SelectedIndexChanged);
            // 
            // btnSort3
            // 
            this.btnSort3.BackColor = System.Drawing.Color.Transparent;
            this.btnSort3.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnSort3.Image = global::ToothPick.Properties.Resources.AtoZ;
            this.btnSort3.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSort3.Location = new System.Drawing.Point(827, 6);
            this.btnSort3.Name = "btnSort3";
            this.btnSort3.Size = new System.Drawing.Size(28, 28);
            this.btnSort3.TabIndex = 9;
            this.btnSort3.Text = "radToggleButton1";
            this.btnSort3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.btnSort3_ToggleStateChanged);
            // 
            // ddlSort3
            // 
            this.ddlSort3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ddlSort3.DropDownAnimationEnabled = false;
            this.ddlSort3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSort3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem20.Text = "Fname";
            radListDataItem21.Text = "Lname";
            radListDataItem22.Text = "TchFname";
            radListDataItem23.Text = "TchLname";
            radListDataItem24.Text = "Lang";
            radListDataItem25.Text = "DOB";
            radListDataItem26.Text = "Gen";
            radListDataItem27.Text = "Grd";
            radListDataItem28.Text = "HmRm";
            radListDataItem29.Text = "Special";
            radListDataItem30.Text = "Recess";
            radListDataItem31.Text = "Notes";
            radListDataItem32.Text = "R";
            radListDataItem33.Text = "G";
            radListDataItem34.Text = "B";
            radListDataItem35.Text = "Lunch";
            this.ddlSort3.Items.Add(radListDataItem20);
            this.ddlSort3.Items.Add(radListDataItem21);
            this.ddlSort3.Items.Add(radListDataItem22);
            this.ddlSort3.Items.Add(radListDataItem23);
            this.ddlSort3.Items.Add(radListDataItem24);
            this.ddlSort3.Items.Add(radListDataItem25);
            this.ddlSort3.Items.Add(radListDataItem26);
            this.ddlSort3.Items.Add(radListDataItem27);
            this.ddlSort3.Items.Add(radListDataItem28);
            this.ddlSort3.Items.Add(radListDataItem29);
            this.ddlSort3.Items.Add(radListDataItem30);
            this.ddlSort3.Items.Add(radListDataItem31);
            this.ddlSort3.Items.Add(radListDataItem32);
            this.ddlSort3.Items.Add(radListDataItem33);
            this.ddlSort3.Items.Add(radListDataItem34);
            this.ddlSort3.Items.Add(radListDataItem35);
            this.ddlSort3.Location = new System.Drawing.Point(668, 5);
            this.ddlSort3.Name = "ddlSort3";
            this.ddlSort3.Size = new System.Drawing.Size(143, 31);
            this.ddlSort3.TabIndex = 8;
            this.ddlSort3.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.ddlSort3_SelectedIndexChanged);
            // 
            // btnSort1
            // 
            this.btnSort1.BackColor = System.Drawing.Color.Transparent;
            this.btnSort1.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnSort1.Image = global::ToothPick.Properties.Resources.AtoZ;
            this.btnSort1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSort1.Location = new System.Drawing.Point(421, 6);
            this.btnSort1.Name = "btnSort1";
            this.btnSort1.Size = new System.Drawing.Size(28, 28);
            this.btnSort1.TabIndex = 7;
            this.btnSort1.Text = "radToggleButton1";
            this.btnSort1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.btnSort1_ToggleStateChanged);
            // 
            // ddlSort1
            // 
            this.ddlSort1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ddlSort1.DropDownAnimationEnabled = false;
            this.ddlSort1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSort1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem36.Text = "Status";
            radListDataItem37.Text = "Fname";
            radListDataItem38.Text = "Lname";
            radListDataItem39.Text = "TchFname";
            radListDataItem40.Text = "TchLname";
            radListDataItem41.Text = "Lang";
            radListDataItem42.Text = "DOB";
            radListDataItem43.Text = "Gen";
            radListDataItem44.Text = "Grd";
            radListDataItem45.Text = "HmRm";
            radListDataItem46.Text = "Special";
            radListDataItem47.Text = "Recess";
            radListDataItem48.Text = "Notes";
            radListDataItem49.Text = "R";
            radListDataItem50.Text = "G";
            radListDataItem51.Text = "B";
            radListDataItem52.Text = "Lunch";
            this.ddlSort1.Items.Add(radListDataItem36);
            this.ddlSort1.Items.Add(radListDataItem37);
            this.ddlSort1.Items.Add(radListDataItem38);
            this.ddlSort1.Items.Add(radListDataItem39);
            this.ddlSort1.Items.Add(radListDataItem40);
            this.ddlSort1.Items.Add(radListDataItem41);
            this.ddlSort1.Items.Add(radListDataItem42);
            this.ddlSort1.Items.Add(radListDataItem43);
            this.ddlSort1.Items.Add(radListDataItem44);
            this.ddlSort1.Items.Add(radListDataItem45);
            this.ddlSort1.Items.Add(radListDataItem46);
            this.ddlSort1.Items.Add(radListDataItem47);
            this.ddlSort1.Items.Add(radListDataItem48);
            this.ddlSort1.Items.Add(radListDataItem49);
            this.ddlSort1.Items.Add(radListDataItem50);
            this.ddlSort1.Items.Add(radListDataItem51);
            this.ddlSort1.Items.Add(radListDataItem52);
            this.ddlSort1.Location = new System.Drawing.Point(262, 5);
            this.ddlSort1.Name = "ddlSort1";
            this.ddlSort1.Size = new System.Drawing.Size(143, 31);
            this.ddlSort1.TabIndex = 4;
            this.ddlSort1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.ddlSort1_SelectedIndexChanged);
            // 
            // lblSort
            // 
            this.lblSort.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblSort.Location = new System.Drawing.Point(205, 8);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(41, 25);
            this.lblSort.TabIndex = 3;
            this.lblSort.Text = "Sort:";
            // 
            // schoolsTableAdapter
            // 
            this.schoolsTableAdapter.ClearBeforeFill = true;
            // 
            // studentTableTableAdapter
            // 
            this.studentTableTableAdapter.ClearBeforeFill = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 600000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chkLunchColumnVisible
            // 
            this.chkLunchColumnVisible.Location = new System.Drawing.Point(0, 0);
            this.chkLunchColumnVisible.Name = "chkLunchColumnVisible";
            this.chkLunchColumnVisible.Size = new System.Drawing.Size(100, 18);
            this.chkLunchColumnVisible.TabIndex = 0;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnRefreshSchools;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1249, 690);
            this.Controls.Add(this.pnlStudentPickList);
            this.Controls.Add(this.pnlChildSearch);
            this.Controls.Add(this.pnlSchoolYear);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Text = "Tooth Pick";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.pnlSchoolYear.PanelContainer.ResumeLayout(false);
            this.pnlSchoolYear.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSchoolYear)).EndInit();
            this.pnlSchoolYear.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataRefreshTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefreshSchools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefreshStudents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSchools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schoolsDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSchool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFullScreen)).EndInit();
            this.pnlChildSearch.PanelContainer.ResumeLayout(false);
            this.pnlChildSearch.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlChildSearch)).EndInit();
            this.pnlChildSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkLunchTimeColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkYColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCommentsColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTeacherFirstColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHomeRoomColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSpecialTimeColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecessColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGenderColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDOBColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLanguageColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTeacherLastColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFirstNameColumnVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolDDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolHyg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAfterSchoolCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolReferralConsult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolDDS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchoolHyg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAfterSchool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientWheelchair)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtComplexPatientCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientSpecialNeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientGagReflex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientLimitedOpening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientAnesthetic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatientBehavior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkComplexPatient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHygienistSealant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHygienistCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistSealant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistPX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistPX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistHygieneFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienistFluoride)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHygienist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frpDentist)).EndInit();
            this.frpDentist.ResumeLayout(false);
            this.frpDentist.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDentistCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLRAnes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistCustom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLLAnes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistIntake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistULAnes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistURAnes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistLL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistUL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentistUR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDentist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade5th)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade4th)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade3rd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade2nd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGrade1st)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGradeAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilterGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFilterStatus)).EndInit();
            this.pnlStudentPickList.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlStudentPickList)).EndInit();
            this.pnlStudentPickList.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblStudentCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSort1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSort1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLunchColumnVisible)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private Telerik.WinControls.UI.RadCollapsiblePanel pnlSchoolYear;
        private Telerik.WinControls.UI.RadDropDownList cmbSchools;
        private Telerik.WinControls.UI.RadLabel lblSchool;
        private Telerik.WinControls.UI.RadCollapsiblePanel pnlChildSearch;
        private Telerik.WinControls.UI.RadCollapsiblePanel pnlStudentPickList;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadButton btnRefreshSchools;
        private Telerik.WinControls.UI.RadButton btnRefreshStudents;
        private Telerik.WinControls.UI.RadDropDownList cmbStatus;
        private Telerik.WinControls.UI.RadLabel lblFilterStatus;
        private Telerik.WinControls.UI.RadLabel lblFilterGrade;
        private Telerik.WinControls.UI.RadCheckBox chkGrade5th;
        private Telerik.WinControls.UI.RadCheckBox chkGrade4th;
        private Telerik.WinControls.UI.RadCheckBox chkGrade3rd;
        private Telerik.WinControls.UI.RadCheckBox chkGrade2nd;
        private Telerik.WinControls.UI.RadCheckBox chkGrade1st;
        private Telerik.WinControls.UI.RadCheckBox chkGradeK;
        private Telerik.WinControls.UI.RadCheckBox chkGradeAll;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadTextBox txtAfterSchoolHyg;
        private Telerik.WinControls.UI.RadTextBox txtAfterSchoolCustom;
        private Telerik.WinControls.UI.RadCheckBox chkAfterSchoolCustom;
        private Telerik.WinControls.UI.RadCheckBox chkAfterSchoolReferralConsult;
        private Telerik.WinControls.UI.RadCheckBox chkAfterSchoolDDS;
        private Telerik.WinControls.UI.RadCheckBox chkAfterSchoolHyg;
        private Telerik.WinControls.UI.RadCheckBox chkAfterSchool;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientWheelchair;
        private Telerik.WinControls.UI.RadTextBox txtComplexPatientCustom;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientCustom;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientSpecialNeeds;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientGagReflex;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientLimitedOpening;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientAnesthetic;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatientBehavior;
        private Telerik.WinControls.UI.RadCheckBox chkComplexPatient;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox txtHygienistSealant;
        private Telerik.WinControls.UI.RadTextBox txtHygienistCustom;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistCustom;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistSealant;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistPX2;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistPX1;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistHygieneFirst;
        private Telerik.WinControls.UI.RadCheckBox chkHygienistFluoride;
        private Telerik.WinControls.UI.RadCheckBox chkHygienist;
        private Telerik.WinControls.UI.RadGroupBox frpDentist;
        private Telerik.WinControls.UI.RadTextBox txtDentistCustom;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadCheckBox chkDentistLRAnes;
        private Telerik.WinControls.UI.RadCheckBox chkDentistCustom;
        private Telerik.WinControls.UI.RadCheckBox chkDentistLLAnes;
        private Telerik.WinControls.UI.RadCheckBox chkDentistIntake;
        private Telerik.WinControls.UI.RadCheckBox chkDentistULAnes;
        private Telerik.WinControls.UI.RadCheckBox chkDentistLR;
        private Telerik.WinControls.UI.RadCheckBox chkDentistURAnes;
        private Telerik.WinControls.UI.RadCheckBox chkDentistLL;
        private Telerik.WinControls.UI.RadCheckBox chkDentistUL;
        private Telerik.WinControls.UI.RadCheckBox chkDentistUR;
        private Telerik.WinControls.UI.RadCheckBox chkDentist;
        private Telerik.WinControls.UI.RadTextBox txtAfterSchoolDDS;
        private Telerik.WinControls.UI.RadLabel lblDataRefreshTime;
        private Telerik.WinControls.UI.RadButton btnFullScreen;
        private Telerik.WinControls.UI.RadCheckBox chkHomeRoomColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkSpecialTimeColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkRecessColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkGradeColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkGenderColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkDOBColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkLanguageColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkTeacherLastColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkFirstNameColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkTeacherFirstColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkBColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkYColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkGColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkRColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkCommentsColumnVisible;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadDropDownList ddlSort1;
        private Telerik.WinControls.UI.RadLabel lblSort;
        private Telerik.WinControls.UI.RadToggleButton btnSort1;
        private Telerik.WinControls.UI.RadToggleButton btnSort2;
        private Telerik.WinControls.UI.RadDropDownList ddlSort2;
        private Telerik.WinControls.UI.RadToggleButton btnSort3;
        private Telerik.WinControls.UI.RadDropDownList ddlSort3;
        private SchoolsDataset schoolsDataset;
        private System.Windows.Forms.BindingSource schoolsBindingSource;
        private SchoolsDatasetTableAdapters.SchoolsTableAdapter schoolsTableAdapter;
        private System.Windows.Forms.BindingSource studentDataSetBindingSource;
        private StudentDataSet studentDataSet;
        private System.Windows.Forms.BindingSource studentTableBindingSource;
        private StudentDataSetTableAdapters.StudentTableTableAdapter studentTableTableAdapter;
        private Telerik.WinControls.UI.RadButton btnClearSort;
        private Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadTextBox txtPIN;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadCheckBox chkLunchTimeColumnVisible;
        private Telerik.WinControls.UI.RadCheckBox chkLunchColumnVisible;
        private Telerik.WinControls.UI.RadLabel lblStudentCount;
    }
}

