﻿namespace ToothPick
{
    public partial class frmMain 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radCollapsiblePanel2 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.lblYear = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.lblSchool = new Telerik.WinControls.UI.RadLabel();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radCollapsiblePanel3 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).BeginInit();
            this.radCollapsiblePanel2.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSchool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).BeginInit();
            this.radCollapsiblePanel3.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // radCollapsiblePanel2
            // 
            this.radCollapsiblePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel2.HeaderText = "Select School/Year";
            this.radCollapsiblePanel2.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel2.Name = "radCollapsiblePanel2";
            this.radCollapsiblePanel2.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 931, 79);
            // 
            // radCollapsiblePanel2.PanelContainer
            // 
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.radDropDownList2);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.lblSchool);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.radDropDownList1);
            this.radCollapsiblePanel2.PanelContainer.Controls.Add(this.lblYear);
            this.radCollapsiblePanel2.PanelContainer.Size = new System.Drawing.Size(929, 51);
            this.radCollapsiblePanel2.Size = new System.Drawing.Size(931, 79);
            this.radCollapsiblePanel2.TabIndex = 4;
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(22, 14);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(63, 18);
            this.lblYear.TabIndex = 0;
            this.lblYear.Text = "Select Year:";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.DropDownAnimationEnabled = false;
            radListDataItem2.Text = "2016-2017";
            radListDataItem3.Text = "2017-2018";
            radListDataItem4.Text = "2018-2019";
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Items.Add(radListDataItem3);
            this.radDropDownList1.Items.Add(radListDataItem4);
            this.radDropDownList1.Location = new System.Drawing.Point(103, 14);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(143, 20);
            this.radDropDownList1.TabIndex = 0;
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.DropDownAnimationEnabled = false;
            radListDataItem1.Text = "ZZZ School";
            this.radDropDownList2.Items.Add(radListDataItem1);
            this.radDropDownList2.Location = new System.Drawing.Point(390, 16);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.Size = new System.Drawing.Size(388, 20);
            this.radDropDownList2.TabIndex = 1;
            // 
            // lblSchool
            // 
            this.lblSchool.Location = new System.Drawing.Point(309, 16);
            this.lblSchool.Name = "lblSchool";
            this.lblSchool.Size = new System.Drawing.Size(75, 18);
            this.lblSchool.TabIndex = 2;
            this.lblSchool.Text = "Select School:";
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCollapsiblePanel1.HeaderText = "Patients Search";
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 79);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 910, 242);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.pictureBox1);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(929, 214);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(931, 242);
            this.radCollapsiblePanel1.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ToothPick.Properties.Resources.SearchDialog1;
            this.pictureBox1.Location = new System.Drawing.Point(-3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(908, 212);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // radCollapsiblePanel3
            // 
            this.radCollapsiblePanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radCollapsiblePanel3.HeaderText = "Student Pick List";
            this.radCollapsiblePanel3.Location = new System.Drawing.Point(0, 321);
            this.radCollapsiblePanel3.Name = "radCollapsiblePanel3";
            this.radCollapsiblePanel3.OwnerBoundsCache = new System.Drawing.Rectangle(0, 321, 931, 369);
            // 
            // radCollapsiblePanel3.PanelContainer
            // 
            this.radCollapsiblePanel3.PanelContainer.Controls.Add(this.radGridView1);
            this.radCollapsiblePanel3.PanelContainer.Size = new System.Drawing.Size(929, 341);
            this.radCollapsiblePanel3.Size = new System.Drawing.Size(931, 369);
            this.radCollapsiblePanel3.TabIndex = 6;
            // 
            // radGridView1
            // 
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(929, 341);
            this.radGridView1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(931, 690);
            this.Controls.Add(this.radCollapsiblePanel3);
            this.Controls.Add(this.radCollapsiblePanel1);
            this.Controls.Add(this.radCollapsiblePanel2);
            this.Name = "Form1";
            this.Text = "Tooth Pick";
            this.radCollapsiblePanel2.PanelContainer.ResumeLayout(false);
            this.radCollapsiblePanel2.PanelContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel2)).EndInit();
            this.radCollapsiblePanel2.ResumeLayout(false);
            this.radCollapsiblePanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSchool)).EndInit();
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.radCollapsiblePanel3.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel3)).EndInit();
            this.radCollapsiblePanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadLabel lblSchool;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel lblYear;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel3;
        private Telerik.WinControls.UI.RadGridView radGridView1;
    }
}

