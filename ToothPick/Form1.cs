﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using ToothPick.Properties;
using System.Diagnostics;
using System.IO;
using System.Data.Common;
using System.Collections;
using System.Threading;

namespace ToothPick
{
    public partial class Form1 : Form
    {
        #region ModuleVariables

        private string strPassword = "879451";

        string m_bSyncDateTime = "";

        int m_nCurrentSchoolIndex = -1;

        #endregion

        #region FormEvents
        public Form1()
        {
            InitializeComponent();
            cmbSchools.DisplayMember = "SchoolName";
            cmbSchools.ValueMember = "SchoolId";

            ConfigureGrid();

            pnlStudentPickList.CollapsiblePanelElement.HeaderElement.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            RestoreMDW();

            if (File.Exists(ConfigurationManager.AppSettings["DatabasePath"].ToString()))
            {
                // TODO: This line of code loads data into the 'schoolsDataset.Schools' table. You can move, or remove it, as needed.
                this.schoolsTableAdapter.Fill(this.schoolsDataset.Schools);
                //if (cmbSchools.Items.Count == 0)
                //{
                //    RefreshSchools();
                //    this.schoolsTableAdapter.Fill(this.schoolsDataset.Schools);
                //}

                // TODO: This line of code loads data into the 'studentDataSet.StudentTable' table. You can move, or remove it, as needed.
                this.studentTableTableAdapter.Fill(this.studentDataSet.StudentTable);
                this.radGridView1.EnableFiltering = true;
                this.radGridView1.ShowFilteringRow = false;
            }
            else
            {
                RestoreMDB(ConfigurationManager.AppSettings["DatabasePath"].ToString());
                this.schoolsTableAdapter.Fill(this.schoolsDataset.Schools);
            }

            LoadSettings();

            ShowPIN();

        }

        private void ShowPIN()
        {
            SaveSettings();
            timer1.Stop();
            pnlChildSearch.Visible = false;
            pnlStudentPickList.Visible = false;
            txtPIN.Visible = true;
            txtPIN.Text = "";
            lblSchool.Text = "Enter PIN";
            btnRefreshSchools.Text = "Submit PIN";
            btnRefreshStudents.Visible = false;
            cmbSchools.Visible = false;
            pnlSchoolYear.IsExpanded = true;
            lblDataRefreshTime.Visible = false;
            pnlSchoolYear.HeaderText = "Enter PIN";
            txtPIN.Focus();
        }

        private void HidePIN()
        {
            pnlChildSearch.Visible = true;
            pnlStudentPickList.Visible = true;
            lblSchool.Text = "Select School:";
            btnRefreshSchools.Text = "Refresh Schools";
            btnRefreshStudents.Visible = true;
            txtPIN.Visible = false;
            lblDataRefreshTime.Visible = true;
            pnlSchoolYear.HeaderText = "School Information";
            LoadSettings();
            cmbSchools.Visible = true;

            new Thread(() => { resizeMe(); }).Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }
        #endregion

        #region ControlEvents

        private void btnRefreshSchools_Click(object sender, EventArgs e)
        {
            if (txtPIN.Visible)
            {
                if (txtPIN.Text != strPassword)
                {
                    MessageBox.Show("Incorrect PIN entered.");
                    txtPIN.Text = "";
                    return;
                }
                HidePIN();
            }
            else
            {
                DialogResult dr = MessageBox.Show("Switching schools requires you to be connected to the internet and will refresh all data.  Would you like to continue?", "Download new school", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    RefreshSchools();
                    RefreshStudents();
                    m_nCurrentSchoolIndex = Convert.ToInt16(cmbSchools.SelectedIndex);
                    radGridView1.Refresh();
                    ApplyFilteringtoGrid();
                    m_bSyncDateTime = DateTime.Now.ToString();

                    lblDataRefreshTime.Text = "Last Sync Date: " + m_bSyncDateTime;
                    this.Text = "Toothpick - Last Sync Date: " + m_bSyncDateTime;
                }
                else
                {
                    cmbSchools.SelectedIndex = m_nCurrentSchoolIndex;
                }
            }
        }

        private void btnRefreshStudents_Click(object sender, EventArgs e)
        {

            DialogResult dr = MessageBox.Show("Refreshing students requires you to be connected to the internet and will refresh all data.  Would you like to continue?", "Download Students", MessageBoxButtons.YesNo);

            if (dr == DialogResult.Yes)
            {
                RefreshStudents();
                this.studentTableTableAdapter.Fill(this.studentDataSet.StudentTable);
                m_bSyncDateTime = DateTime.Now.ToString();

                lblDataRefreshTime.Text = "Last Sync Date: " + m_bSyncDateTime;
                this.Text = "Toothpick - Last Sync Date: " + m_bSyncDateTime;
                ApplyFilteringtoGrid();
            }
            else
            {
                cmbSchools.SelectedIndex = m_nCurrentSchoolIndex;
            }

        }

        private void cmbSchools_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

            if (m_nCurrentSchoolIndex != -1) //Initial Load
            {
                DialogResult dr = MessageBox.Show("Switching schools requires you to be connected to the internet and will refresh all data.  Would you like to continue?", "Download new school", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    RefreshStudents();
                    m_nCurrentSchoolIndex = Convert.ToInt16(cmbSchools.SelectedIndex);
                    radGridView1.Refresh();
                    ApplyFilteringtoGrid();
                    m_bSyncDateTime = DateTime.Now.ToString();

                    lblDataRefreshTime.Text = "Last Sync Date: " + m_bSyncDateTime;
                    this.Text = "Toothpick - Last Sync Date: " + m_bSyncDateTime;
                }
                else
                {
                    cmbSchools.SelectedIndex = m_nCurrentSchoolIndex;
                }
            }else
                ApplyFilteringtoGrid();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void chkGradeAll_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (chkGradeAll.Checked)
            {
                chkGradeK.Checked = true;
                chkGrade1st.Checked = true;
                chkGrade2nd.Checked = true;
                chkGrade3rd.Checked = true;
                chkGrade4th.Checked = true;
                chkGrade5th.Checked = true;
            }
            else
            {
                chkGradeK.Checked = false;
                chkGrade1st.Checked = false;
                chkGrade2nd.Checked = false;
                chkGrade3rd.Checked = false;
                chkGrade4th.Checked = false;
                chkGrade5th.Checked = false;
            }

            ApplyFilteringtoGrid();
        }

        private void chkGradeK_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGradeK.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();

        }

        private void chkGrade1st_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGrade1st.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();
        }

        private void chkGrade2nd_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGrade2nd.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();
        }

        private void chkGrade3rd_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGrade3rd.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();
        }

        private void chkGrade4th_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGrade4th.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();
        }

        private void chkGrade5th_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (!chkGrade5th.Checked)
            {
                chkGradeAll.ToggleStateChanged -= chkGradeAll_ToggleStateChanged;
                chkGradeAll.Checked = false;
                chkGradeAll.ToggleStateChanged += chkGradeAll_ToggleStateChanged;
            }
            else
            {
                if (chkGradeK.Checked && chkGrade1st.Checked && chkGrade2nd.Checked && chkGrade3rd.Checked && chkGrade4th.Checked && chkGrade5th.Checked)
                {
                    chkGradeAll.Checked = true;
                }
            }
            ApplyFilteringtoGrid();
        }


        private void chkDentist_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (chkDentist.Checked)
            {
                chkDentistUR.Checked = false;
                chkDentistUR.Enabled = false;
                chkDentistURAnes.Checked = false;
                chkDentistURAnes.Enabled = false;
                chkDentistUL.Checked = false;
                chkDentistUL.Enabled = false;
                chkDentistULAnes.Checked = false;
                chkDentistULAnes.Enabled = false;
                chkDentistLL.Checked = false;
                chkDentistLL.Enabled = false;
                chkDentistLLAnes.Checked = false;
                chkDentistLLAnes.Enabled = false;
                chkDentistLR.Checked = false;
                chkDentistLR.Enabled = false;
                chkDentistLRAnes.Checked = false;
                chkDentistLRAnes.Enabled = false;
                chkDentistIntake.Checked = false;
                chkDentistIntake.Enabled = false;
                chkDentistCustom.Checked = false;
                chkDentistCustom.Enabled = false;
                txtDentistCustom.Enabled = false;
            }
            else
            {
                chkDentistUR.Checked = false;
                chkDentistUR.Enabled = true;
                chkDentistURAnes.Checked = false;
                chkDentistURAnes.Enabled = true;
                chkDentistUL.Checked = false;
                chkDentistUL.Enabled = true;
                chkDentistULAnes.Checked = false;
                chkDentistULAnes.Enabled = true;
                chkDentistLL.Checked = false;
                chkDentistLL.Enabled = true;
                chkDentistLLAnes.Checked = false;
                chkDentistLLAnes.Enabled = true;
                chkDentistLR.Checked = false;
                chkDentistLR.Enabled = true;
                chkDentistLRAnes.Checked = false;
                chkDentistLRAnes.Enabled = true;
                chkDentistIntake.Checked = false;
                chkDentistIntake.Enabled = true;
                chkDentistCustom.Checked = false;
                chkDentistCustom.Enabled = true;
                txtDentistCustom.Enabled = true;
            }
            ApplyFilteringtoGrid();
        }

        private void chkDentistUR_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistUL_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistLL_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistLR_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistIntake_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistCustom_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistURAnes_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistULAnes_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistLLAnes_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkDentistLRAnes_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienist_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (chkHygienist.Checked)
            {
                chkHygienistFluoride.Checked = false;
                chkHygienistFluoride.Enabled = false;
                chkHygienistHygieneFirst.Checked = false;
                chkHygienistHygieneFirst.Enabled = false;
                chkHygienistPX1.Checked = false;
                chkHygienistPX1.Enabled = false;
                chkHygienistPX2.Checked = false;
                chkHygienistPX2.Enabled = false;
                chkHygienistSealant.Checked = false;
                chkHygienistSealant.Enabled = false;
                txtHygienistSealant.Enabled = false;
                chkHygienistCustom.Checked = false;
                chkHygienistCustom.Enabled = false;
                txtHygienistCustom.Enabled = false;
            }
            else
            {
                chkHygienistFluoride.Checked = false;
                chkHygienistFluoride.Enabled = true;
                chkHygienistHygieneFirst.Checked = false;
                chkHygienistHygieneFirst.Enabled = true;
                chkHygienistPX1.Checked = false;
                chkHygienistPX1.Enabled = true;
                chkHygienistPX2.Checked = false;
                chkHygienistPX2.Enabled = true;
                chkHygienistSealant.Checked = false;
                chkHygienistSealant.Enabled = true;
                txtHygienistSealant.Enabled = true;
                chkHygienistCustom.Checked = false;
                chkHygienistCustom.Enabled = true;
                txtHygienistCustom.Enabled = true;
            }
            ApplyFilteringtoGrid();
        }

        private void chkHygienistFluoride_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienistHygieneFirst_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienistPX1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienistPX2_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienistSealant_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkHygienistCustom_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatient_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (chkComplexPatient.Checked)
            {
                chkComplexPatientBehavior.Checked = false;
                chkComplexPatientBehavior.Enabled = false;
                chkComplexPatientAnesthetic.Checked = false;
                chkComplexPatientAnesthetic.Enabled = false;
                chkComplexPatientLimitedOpening.Checked = false;
                chkComplexPatientLimitedOpening.Enabled = false;
                chkComplexPatientGagReflex.Checked = false;
                chkComplexPatientGagReflex.Enabled = false;
                chkComplexPatientSpecialNeeds.Checked = false;
                chkComplexPatientSpecialNeeds.Enabled = false;
                chkComplexPatientWheelchair.Checked = false;
                chkComplexPatientWheelchair.Enabled = false;
                chkComplexPatientCustom.Checked = false;
                chkComplexPatientCustom.Enabled = false;
                txtComplexPatientCustom.Enabled = false;

            }
            else
            {
                chkComplexPatientBehavior.Checked = false;
                chkComplexPatientBehavior.Enabled = true;
                chkComplexPatientAnesthetic.Checked = false;
                chkComplexPatientAnesthetic.Enabled = true;
                chkComplexPatientLimitedOpening.Checked = false;
                chkComplexPatientLimitedOpening.Enabled = true;
                chkComplexPatientGagReflex.Checked = false;
                chkComplexPatientGagReflex.Enabled = true;
                chkComplexPatientSpecialNeeds.Checked = false;
                chkComplexPatientSpecialNeeds.Enabled = true;
                chkComplexPatientWheelchair.Checked = false;
                chkComplexPatientWheelchair.Enabled = true;
                chkComplexPatientCustom.Checked = false;
                chkComplexPatientCustom.Enabled = true;
                txtComplexPatientCustom.Enabled = true;
            }
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientBehavior_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientAnesthetic_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientLimitedOpening_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientGagReflex_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientSpecialNeeds_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientWheelchair_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkComplexPatientCustom_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkAfterSchool_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (chkAfterSchool.Checked)
            {
                chkAfterSchoolHyg.Checked = false;
                chkAfterSchoolHyg.Enabled = false;
                txtAfterSchoolHyg.Enabled = false;
                chkAfterSchoolDDS.Checked = false;
                chkAfterSchoolDDS.Enabled = false;
                txtAfterSchoolDDS.Enabled = false;
                chkAfterSchoolReferralConsult.Checked = false;
                chkAfterSchoolCustom.Checked = false;
                chkAfterSchoolCustom.Enabled = false;
                txtAfterSchoolCustom.Enabled = false;
            }
            else
            {
                chkAfterSchoolHyg.Checked = false;
                chkAfterSchoolHyg.Enabled = true;
                txtAfterSchoolHyg.Enabled = true;
                chkAfterSchoolDDS.Checked = false;
                chkAfterSchoolDDS.Enabled = true;
                txtAfterSchoolDDS.Enabled = true;
                chkAfterSchoolReferralConsult.Checked = false;
                chkAfterSchoolReferralConsult.Enabled = true;
                chkAfterSchoolCustom.Checked = false;
                chkAfterSchoolCustom.Enabled = true;
                txtAfterSchoolCustom.Enabled = true;
            }
            ApplyFilteringtoGrid();
        }

        private void chkAfterSchoolHyg_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkAfterSchoolDDS_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkAfterSchoolRefferalConsult_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void chkAfterSchoolCustom_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            ApplyFilteringtoGrid();
        }

        private void btnFullScreen_Click(object sender, EventArgs e)
        {
            if (btnFullScreen.Text.ToUpper()== "EXIT FULL SCREEN")
            {
                btnFullScreen.Text = "Full Screen";
                pnlSchoolYear.Visible = true;
                pnlChildSearch.Visible = true;
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
            else
            {
                btnFullScreen.Text = "Exit Full Screen";
                pnlSchoolYear.Visible = false;
                pnlChildSearch.Visible = false;
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.None;
            }
            new Thread(() => { resizeMe(); }).Start();
        }

        private void radGridView1_DoubleClick(object sender, EventArgs e)
        {
            pnlSchoolYear.Visible = true;
            pnlChildSearch.Visible = true;
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.Sizable;
            btnFullScreen.Text = "Full Screen";
            new Thread(() => { resizeMe(); }).Start();
        }

        private void chkFirstNameColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["StudentFirstName"].IsVisible = chkFirstNameColumnVisible.Checked;
        }

        private void chkTeacherFirstColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["TeacherFirstname"].IsVisible = chkTeacherFirstColumnVisible.Checked;
        }

        private void chkTeacherLastColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["TeacherLastName"].IsVisible = chkTeacherLastColumnVisible.Checked;
        }

        private void chkLanguageColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["Language"].IsVisible = chkLanguageColumnVisible.Checked;
        }

        private void chkDOBColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["DOB"].IsVisible = chkDOBColumnVisible.Checked;
        }

        private void chkGenderColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["Gender"].IsVisible = chkGenderColumnVisible.Checked;
        }

        private void chkGrade_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["Grade"].IsVisible = chkGradeColumnVisible.Checked;
        }

        private void chkHomeRoomColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["HomeRoom"].IsVisible = chkHomeRoomColumnVisible.Checked;
        }

        private void chkSpecialTimeColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["SpecialTime"].IsVisible = chkSpecialTimeColumnVisible.Checked;
        }

        private void chkRecessColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["RecessTime"].IsVisible = chkRecessColumnVisible.Checked;
        }

        private void chkLunchTimeColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["LunchTime"].IsVisible = chkLunchTimeColumnVisible.Checked;
        }

        private void chkCommentsColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["Comments"].IsVisible = chkCommentsColumnVisible.Checked;
        }

        private void chkRColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["R"].IsVisible = chkRColumnVisible.Checked;
        }

        private void chkGColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["G"].IsVisible = chkGColumnVisible.Checked;
        }

        private void chkYColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["Y"].IsVisible = chkYColumnVisible.Checked;
        }

        private void chkBColumnVisible_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.Columns["B"].IsVisible = chkBColumnVisible.Checked;
        }

        private void ddlSort1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            SortGrid();

        }

        private void btnSort1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortGrid();
        }

        private void ddlSort2_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            SortGrid();
        }

        private void ddlSort3_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            SortGrid();
        }

        private void btnSort2_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortGrid();
        }

        private void btnSort3_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            SortGrid();
        }

        private void radGridView1_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.DrawFill = false;

            if (e.CellElement.ColumnInfo.Name == "R")
            {
                e.CellElement.DrawFill = true;
                e.CellElement.ForeColor = Color.Black;
                e.CellElement.NumberOfColors = 1;
                e.CellElement.BackColor = Color.FromArgb(255, 120, 120);
            }

            if (e.CellElement.ColumnInfo.Name == "G")
            {
                e.CellElement.DrawFill = true;
                e.CellElement.ForeColor = Color.Black;
                e.CellElement.NumberOfColors = 1;
                e.CellElement.BackColor = Color.FromArgb(120, 255, 120);
            }
            if (e.CellElement.ColumnInfo.Name == "Y")
            {
                e.CellElement.DrawFill = true;
                e.CellElement.ForeColor = Color.Black;
                e.CellElement.NumberOfColors = 1;
                e.CellElement.BackColor = Color.FromArgb(255, 255, 120);
            }
            if (e.CellElement.ColumnInfo.Name == "B")
            {
                e.CellElement.DrawFill = true;
                e.CellElement.ForeColor = Color.Black;
                e.CellElement.NumberOfColors = 1;
                e.CellElement.BackColor = Color.FromArgb(180, 180, 255);
            }

        }

        private void btnClearSort_Click(object sender, EventArgs e)
        {
            ddlSort1.SelectedItem = null;
            ddlSort2.SelectedItem = null;
            ddlSort3.SelectedItem = null;

            SortGrid();
        }

        private void radGridView1_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            //Console.Write("123");

            //GridViewRowInfo dr = e.Row;
            //Console.Write(dr.Index);

            //GridDataItem 

            //this.radGridView1.Rows[dr.Index]
        }

        private void radGridView1_RowFormatting(object sender, RowFormattingEventArgs e)
        {

            //GridViewRowInfo dr = this.radGridView1.Rows[e.RowElement.RowInfo.Index];
            ////GridViewRowInfo dr = this.radGridView1.Rows[1];

            //DataRowView drv = (DataRowView)dr.DataBoundItem;
            //StudentDataSet.StudentTableRow dr1 = (StudentDataSet.StudentTableRow)drv.Row;
            if (e.RowElement.RowInfo.Cells["Status"].Value != null)
            {

                if (e.RowElement.RowInfo.Cells["Status"].Value.ToString().Trim() != "Available")
                {
                    e.RowElement.DrawFill = true;
                    e.RowElement.BackColor = Color.DarkGray;
                }
                else
                {
                    e.RowElement.DrawFill = true;
                    e.RowElement.BackColor = Color.White;
                }
            }
        }

        private void radGridView1_ValueChanged(object sender, EventArgs e)
        {
            Debug.Write("111");

            RadDropDownListEditor ci = (RadDropDownListEditor)sender;

            string index = this.radGridView1.SelectedRows[0].Cells["StudentID"].Value.ToString();

            long studentID = Convert.ToInt64(index);

            UpdateStudentStatus(studentID, ci.Value.ToString());
        }

        private void radGridView1_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            var editor = e.ActiveEditor;
            if (editor != null && editor is RadDropDownListEditor)
            {
                RadDropDownListEditor dropDown = (RadDropDownListEditor)editor;
                RadDropDownListEditorElement element = (RadDropDownListEditorElement)dropDown.EditorElement;
                element.ListElement.Font = new Font("Arial", 14f);
                element.ListElement.ItemHeight = 25;
                element.ShowPopup();
            }
        }

        private void txtDentistCustom_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtHygienistCustom_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtHygienistSealant_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtComplexPatientCustom_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtAfterSchoolHyg_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtAfterSchoolDDS_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void txtAfterSchoolCustom_TextChanging(object sender, Telerik.WinControls.TextChangingEventArgs e)
        {
            ApplyFilteringtoGrid();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ShowPIN();
        }

        private void txtPIN_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            ResetTimer();
        }

        private void radGridView1_MouseMove(object sender, MouseEventArgs e)
        {
            ResetTimer();
        }

        private void chkDentistIntake_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region HelperMethods

        private void RestoreMDW()
        {
            string strAccessPath = Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Access";
            string strMDWPath = strAccessPath + "\\system.mdw";

            if (!Directory.Exists(strAccessPath))
                Directory.CreateDirectory(strAccessPath);

            if (!File.Exists(strMDWPath))
            {
                
                string strSourceMDW = Directory.GetCurrentDirectory() + "\\system.mdw";
                File.Copy(strSourceMDW, strMDWPath);
            }
        }

        private void RestoreMDB(string strPAth)
        {

            string strCurrrentDirectory = Directory.GetCurrentDirectory();
            DirectoryInfo strDIRPath = new DirectoryInfo(strPAth);
            if (!Directory.Exists(strDIRPath.Parent.FullName))
            {
                Directory.CreateDirectory(strDIRPath.Parent.FullName);
            }


            if (strCurrrentDirectory != strPAth && !File.Exists(strDIRPath.Parent.FullName))
            {
                File.Copy(strCurrrentDirectory + "\\toothpick.mdb", strDIRPath.Parent.FullName + "\\toothpick.mdb");
                //RefreshSchools();
                ShowPIN();
                MessageBox.Show("Toothpick has been configured properly.  Please click \"Refresh Schools\" to downlaod the list of schools.");
            }
        }

        private void ConfigureGrid()
        {
            ResetTimer();

            cmbSchools.ListElement.Font = new Font("Arial", 14f);
            cmbSchools.ListElement.ItemHeight = 25;
            cmbStatus.ListElement.Font = new Font("Arial", 14f);
            cmbStatus.ListElement.ItemHeight = 25;


            ddlSort1.ListElement.Font = new Font("Arial", 14f);
            ddlSort1.ListElement.ItemHeight = 25;
            ddlSort2.ListElement.Font = new Font("Arial", 14f);
            ddlSort2.ListElement.ItemHeight = 25;
            ddlSort3.ListElement.Font = new Font("Arial", 14f);
            ddlSort3.ListElement.ItemHeight = 25;

            this.radGridView1.Columns["StudentID"].IsVisible = false;
            this.radGridView1.Columns["SchoolID"].IsVisible = false;
            this.radGridView1.Columns["ActiveDate"].IsVisible = false;
            this.radGridView1.Columns["treatment_accepted"].IsVisible = false;
            this.radGridView1.Columns["completion_date"].IsVisible = false;

            this.radGridView1.Columns["Status"].Width = 150;

            this.radGridView1.Columns["StudentFirstName"].Width = 100;
            this.radGridView1.Columns["StudentFirstName"].HeaderText = "Fname";
            this.radGridView1.Columns["StudentLastName"].Width = 100;
            this.radGridView1.Columns["StudentLastName"].HeaderText = "Lname";
            this.radGridView1.Columns["TeacherFirstname"].Width = 100;
            this.radGridView1.Columns["TeacherFirstname"].HeaderText = "TchFname";
            this.radGridView1.Columns["TeacherLastName"].Width = 100;
            this.radGridView1.Columns["TeacherLastName"].HeaderText = "TchLname";
            this.radGridView1.Columns["Language"].Width = 50;
            this.radGridView1.Columns["Language"].HeaderText = "Lang";
            this.radGridView1.Columns["DOB"].Width = 100;
            this.radGridView1.Columns["DOB"].HeaderText = "DOB";
            this.radGridView1.Columns["DOB"].FormatString = "{0:d}";

            this.radGridView1.Columns["Gender"].Width = 63;
            this.radGridView1.Columns["Gender"].HeaderText = "Gen";
            this.radGridView1.Columns["Grade"].Width = 54;
            this.radGridView1.Columns["Grade"].HeaderText = "Grd";
            this.radGridView1.Columns["HomeRoom"].Width = 102;
            this.radGridView1.Columns["HomeRoom"].HeaderText = "HmRm";
            this.radGridView1.Columns["SpecialTime"].Width = 104;
            this.radGridView1.Columns["SpecialTime"].HeaderText = "Special";
            this.radGridView1.Columns["RecessTime"].Width = 104;
            this.radGridView1.Columns["RecessTime"].HeaderText = "Recess";
            this.radGridView1.Columns["LunchTime"].Width = 104;
            this.radGridView1.Columns["LunchTime"].HeaderText = "Lunch";
            this.radGridView1.Columns["Comments"].Width = 250;
            this.radGridView1.Columns["Comments"].HeaderText = "Notes";

            radGridView1.AutoSizeRows = true;

            this.radGridView1.Columns["R"].Width = 100;
            this.radGridView1.Columns["R"].HeaderText = "R";
            this.radGridView1.Columns["G"].Width = 100;
            this.radGridView1.Columns["G"].HeaderText = "G";
            this.radGridView1.Columns["Y"].Width = 100;
            this.radGridView1.Columns["Y"].HeaderText = "Y";
            this.radGridView1.Columns["B"].Width = 100;
            this.radGridView1.Columns["B"].HeaderText = "B";


            foreach (GridViewColumn g in this.radGridView1.Columns)
            {
                if (g.HeaderText.ToUpper() != "STATUS")
                {
                    g.ReadOnly = true;
                }
            }
            //System.Drawing.Font fnt = new Font("Arial", 15);
            //this.radGridView1.Font = fnt;

            chkTeacherFirstColumnVisible.Checked = false;
            chkGenderColumnVisible.Checked = false;
            chkRecessColumnVisible.Checked = false;

            GridViewComboBoxColumn comboColumn = (GridViewComboBoxColumn)this.radGridView1.Columns["Status"];
            comboColumn.DataSource = new String[] { " Available", "Pulled", "Absent", "W/D", "Complete" };
        }

        private void LoadSettings()
        {
            string strAccessConnection = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=" + ConfigurationManager.AppSettings["DatabasePath"].ToString() + "; Jet OLEDB:Database Password=SDFDENTAL;Mode=Share Exclusive;Persist Security Info=True;";
            strAccessConnection += "Jet OLEDB:System Database=" +    Environment.GetEnvironmentVariable("APPDATA") +    @"\Microsoft\Access\system.mdw";
            OleDbConnection connect = new OleDbConnection(strAccessConnection);
            //connect.ConnectionString = strAccessConnection;
            connect.Open();

            string selectSQL = "";
            selectSQL = "Select * from Settings";

            if (selectSQL != "")
            {
                try
                {
                    OleDbCommand command = new OleDbCommand(selectSQL, connect);
                    OleDbDataReader reader = command.ExecuteReader();

                    bool bLoadDefaults = true;

                    while (reader.Read())
                    {
                        bLoadDefaults = false;
                        string strValue = reader["cmbYear"].ToString();
                        strValue = reader["cmbSchools"].ToString();
                        if (string.IsNullOrEmpty(strValue))
                            cmbSchools.SelectedIndex = 1;
                        else
                        {
                            cmbSchools.SelectedIndex = Convert.ToInt32(strValue);
                            m_nCurrentSchoolIndex = cmbSchools.SelectedIndex;
                        }
                        strValue = reader["cmbStatus"].ToString();
                        if (string.IsNullOrEmpty(strValue) || Convert.ToInt32(strValue) < 0)
                            cmbStatus.SelectedIndex = 0;
                        else
                            cmbStatus.SelectedIndex = Convert.ToInt32(strValue);

                        //if (string.IsNullOrEmpty(reader["cmbStatus"].ToString())) cmbStatus.SelectedIndex = 1; else cmbStatus.SelectedIndex = Convert.ToInt32(reader["cmbStatus"].ToString());

                        if (string.IsNullOrEmpty(reader["chkGradeAll"].ToString())) chkGradeAll.Checked = true; else chkGradeAll.Checked = Convert.ToBoolean(reader["chkGradeAll"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGradeK"].ToString())) chkGradeK.Checked = true; else chkGradeK.Checked = Convert.ToBoolean(reader["chkGradeK"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGrade1st"].ToString())) chkGrade1st.Checked = true; else chkGrade1st.Checked = Convert.ToBoolean(reader["chkGrade1st"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGrade2nd"].ToString())) chkGrade2nd.Checked = true; else chkGrade2nd.Checked = Convert.ToBoolean(reader["chkGrade2nd"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGrade3rd"].ToString())) chkGrade3rd.Checked = true; else chkGrade3rd.Checked = Convert.ToBoolean(reader["chkGrade3rd"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGrade4th"].ToString())) chkGrade4th.Checked = true; else chkGrade4th.Checked = Convert.ToBoolean(reader["chkGrade4th"].ToString());
                        if (string.IsNullOrEmpty(reader["chkGrade5th"].ToString())) chkGrade5th.Checked = true; else chkGrade5th.Checked = Convert.ToBoolean(reader["chkGrade5th"].ToString());

                        if (string.IsNullOrEmpty(reader["pnlSchoolYear"].ToString())) pnlSchoolYear.IsExpanded = true; else pnlSchoolYear.IsExpanded = Convert.ToBoolean(reader["pnlSchoolYear"].ToString());
                        if (string.IsNullOrEmpty(reader["pnlChildSearch"].ToString())) pnlChildSearch.IsExpanded = true; else pnlChildSearch.IsExpanded = Convert.ToBoolean(reader["pnlChildSearch"].ToString());
                        if (string.IsNullOrEmpty(reader["pnlStudentPickList"].ToString())) pnlStudentPickList.IsExpanded = true; else pnlStudentPickList.IsExpanded = Convert.ToBoolean(reader["pnlStudentPickList"].ToString());

                        if (string.IsNullOrEmpty(reader["lblDataRefreshTime"].ToString())) lblDataRefreshTime.Text = "Please Sync Data Now"; else lblDataRefreshTime.Text = "Last Sync Date: " + Convert.ToDateTime(reader["lblDataRefreshTime"].ToString());
                        if (string.IsNullOrEmpty(reader["lblDataRefreshTime"].ToString())) this.Text = "Toothpick - Please Sync Data Now"; else this.Text = "Toothpick - Last Sync Date: " + Convert.ToDateTime(reader["lblDataRefreshTime"].ToString());

                    }
                    reader.Close();

                    if (bLoadDefaults)
                    {
                        cmbSchools.SelectedIndex = 1;
                        m_nCurrentSchoolIndex = cmbSchools.SelectedIndex;
                        cmbStatus.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {
                    cmbSchools.SelectedIndex = 159;
                    m_nCurrentSchoolIndex = cmbSchools.SelectedIndex;
                    cmbStatus.SelectedIndex = 1;
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }



            ApplyFilteringtoGrid();
        }

        private void SaveSettings()
        {
            ResetTimer();

            string strAccessConnection = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=" + ConfigurationManager.AppSettings["DatabasePath"].ToString() + "; Jet OLEDB:Database Password=SDFDENTAL;Mode=Share Exclusive;Persist Security Info=True;";
            strAccessConnection += "Jet OLEDB:System Database=" + Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Access\system.mdw";

            OleDbConnection connect = new OleDbConnection();
            connect.ConnectionString = strAccessConnection;
            connect.Open();

            string selectSQL = "";
            selectSQL = "Update Settings SET ";
            //selectSQL = selectSQL + "cmbYear='8/1/2019'";
            selectSQL = selectSQL + "cmbSchools=" + cmbSchools.SelectedIndex.ToString();
            selectSQL = selectSQL + ",cmbStatus=" + cmbStatus.SelectedIndex.ToString();
            selectSQL = selectSQL + ",chkGradeAll =" + chkGradeAll.Checked.ToString();
            selectSQL = selectSQL + ",chkGradeK = " + chkGradeK.Checked.ToString();
            selectSQL = selectSQL + ",chkGrade1st = " + chkGrade1st.Checked.ToString();
            selectSQL = selectSQL + ",chkGrade2nd = " + chkGrade2nd.Checked.ToString();
            selectSQL = selectSQL + ",chkGrade3rd = " + chkGrade3rd.Checked.ToString();
            selectSQL = selectSQL + ",chkGrade4th = " + chkGrade4th.Checked.ToString();
            selectSQL = selectSQL + ",chkGrade5th = " + chkGrade5th.Checked.ToString();
            selectSQL = selectSQL + ",pnlSchoolYear = " + pnlSchoolYear.IsExpanded.ToString();
            selectSQL = selectSQL + ",pnlChildSearch = " + pnlChildSearch.IsExpanded.ToString();
            selectSQL = selectSQL + ",pnlStudentPickList = " + pnlStudentPickList.IsExpanded.ToString();
            if (!string.IsNullOrEmpty(m_bSyncDateTime))
            {
                selectSQL = selectSQL + ",lblDataRefreshTime = '" + m_bSyncDateTime + "'";
            }

            if (selectSQL != "")
            {
                try
                {
                    OleDbCommand command = new OleDbCommand(selectSQL, connect);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        private void ResetTimer()
        {
            timer1.Stop();
            timer1.Start();
        }

        private void ApplyFilteringtoGrid()
        {
            ResetTimer();

                this.radGridView1.EnableFiltering = true;
                this.radGridView1.FilterDescriptors.Clear();
                CompositeFilterDescriptor mainfilterSquares = new CompositeFilterDescriptor();

                mainfilterSquares.LogicalOperator = FilterLogicalOperator.Or;
                #region Dentist Filter
                //Dentist Filter
                FilterDescriptor DentistFilterDescriptor = null;
                if (chkDentist.Checked)
                {
                    DentistFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsNotLike, "");
                    CompositeFilterDescriptor dentistFilter = new CompositeFilterDescriptor();
                    dentistFilter.FilterDescriptors.Add(DentistFilterDescriptor);
                    dentistFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistFilter);
                }

                //Dentist UR Filter
                FilterDescriptor DentistURFilterDescriptor = null;
                if (chkDentistUR.Checked || chkDentistURAnes.Checked)
                {
                    if (chkDentistURAnes.Checked)
                        DentistURFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%UR-A%");
                    else
                        DentistURFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%UR%");

                    CompositeFilterDescriptor dentistURFilter = new CompositeFilterDescriptor();
                    dentistURFilter.FilterDescriptors.Add(DentistURFilterDescriptor);
                    dentistURFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistURFilter);
                }

                //Dentist UL Filter
                FilterDescriptor DentistULFilterDescriptor = null;
                if (chkDentistUL.Checked || chkDentistULAnes.Checked)
                {
                    if (chkDentistULAnes.Checked)
                        DentistULFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%UL-A%");
                    else
                        DentistULFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%UL%");

                    CompositeFilterDescriptor dentistULFilter = new CompositeFilterDescriptor();
                    dentistULFilter.FilterDescriptors.Add(DentistULFilterDescriptor);
                    dentistULFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistULFilter);
                }

                //Dentist LL Filter
                FilterDescriptor DentistLLFilterDescriptor = null;
                if (chkDentistLL.Checked || chkDentistLLAnes.Checked)
                {
                    if (chkDentistLLAnes.Checked)
                        DentistLLFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%LL-A%");
                    else
                        DentistLLFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%LL%");

                    CompositeFilterDescriptor dentistLLFilter = new CompositeFilterDescriptor();
                    dentistLLFilter.FilterDescriptors.Add(DentistLLFilterDescriptor);
                    dentistLLFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistLLFilter);
                }

                //Dentist LR Filter
                FilterDescriptor DentistLRFilterDescriptor = null;
                if (chkDentistLR.Checked || chkDentistLRAnes.Checked)
                {
                    if (chkDentistLRAnes.Checked)
                        DentistLRFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%LR-A%");
                    else
                        DentistLRFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%LR%");

                    CompositeFilterDescriptor dentistLRFilter = new CompositeFilterDescriptor();
                    dentistLRFilter.FilterDescriptors.Add(DentistLRFilterDescriptor);
                    dentistLRFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistLRFilter);
                }

                //Dentist Intake Filter
                FilterDescriptor DentistIntakeFilterDescriptor = null;
                if (chkDentistIntake.Checked)
                {
                    DentistIntakeFilterDescriptor = new FilterDescriptor("R", FilterOperator.StartsWith, "I");
                
                    CompositeFilterDescriptor dentistIntakeFilter = new CompositeFilterDescriptor();
                
                dentistIntakeFilter.FilterDescriptors.Add(DentistIntakeFilterDescriptor);
                    dentistIntakeFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistIntakeFilter);
                }

                //Dentist Custom Filter
                FilterDescriptor DentistCustomFilterDescriptor = null;
                if (chkDentistCustom.Checked && !string.IsNullOrEmpty(txtDentistCustom.Text))
                {
                    DentistCustomFilterDescriptor = new FilterDescriptor("R", FilterOperator.IsLike, "%" + txtDentistCustom.Text + "%");
                    CompositeFilterDescriptor dentistCustomFilter = new CompositeFilterDescriptor();
                    dentistCustomFilter.FilterDescriptors.Add(DentistCustomFilterDescriptor);
                    dentistCustomFilter.LogicalOperator = FilterLogicalOperator.And;
                    mainfilterSquares.FilterDescriptors.Add(dentistCustomFilter);
                }

                #endregion

                #region Hygenist Filter

                //Hygenist Filter
                FilterDescriptor HygenistFilterDescriptor = null;
                if (chkHygienist.Checked)
                {
                    HygenistFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsNotLike, "");
                    CompositeFilterDescriptor HygenistFilter = new CompositeFilterDescriptor();
                    HygenistFilter.FilterDescriptors.Add(HygenistFilterDescriptor);
                    HygenistFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygenistFilter);
                }

                FilterDescriptor HygienstFluorideFilterDescriptor = null;
                if (chkHygienistFluoride.Checked)
                {
                    HygienstFluorideFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "%FL%");

                    CompositeFilterDescriptor HygienstFluorideFilter = new CompositeFilterDescriptor();
                    HygienstFluorideFilter.FilterDescriptors.Add(HygienstFluorideFilterDescriptor);
                    HygienstFluorideFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienstFluorideFilter);
                }

                FilterDescriptor HygienstHygieneFirstFilterDescriptor = null;
                if (chkHygienistHygieneFirst.Checked)
                {
                    HygienstHygieneFirstFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "%HygFirst%");

                    CompositeFilterDescriptor HygienstHygieneFirstFilter = new CompositeFilterDescriptor();
                    HygienstHygieneFirstFilter.FilterDescriptors.Add(HygienstHygieneFirstFilterDescriptor);
                    HygienstHygieneFirstFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienstHygieneFirstFilter);
                }


                FilterDescriptor HygienstPX1FilterDescriptor = null;
                if (chkHygienistPX1.Checked)
                {
                    HygienstPX1FilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "%PX1%");

                    CompositeFilterDescriptor HygienstPX1Filter = new CompositeFilterDescriptor();
                    HygienstPX1Filter.FilterDescriptors.Add(HygienstPX1FilterDescriptor);
                    HygienstPX1Filter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienstPX1Filter);
                }


                FilterDescriptor HygienstPX2FilterDescriptor = null;
                if (chkHygienistPX2.Checked)
                {
                    HygienstPX2FilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "%PX2%");

                    CompositeFilterDescriptor HygienstPX2Filter = new CompositeFilterDescriptor();
                    HygienstPX2Filter.FilterDescriptors.Add(HygienstPX2FilterDescriptor);
                    HygienstPX2Filter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienstPX2Filter);
                }


                FilterDescriptor HygienstSealantFilterDescriptor = null;
                if (chkHygienistSealant.Checked )
                {
                    if (string.IsNullOrEmpty(txtHygienistSealant.Text))
                        HygienstSealantFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "S%");
                    else
                        HygienstSealantFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "S-" + txtHygienistSealant.Text + "%");

                    CompositeFilterDescriptor HygienstSealantFilter = new CompositeFilterDescriptor();
                    HygienstSealantFilter.FilterDescriptors.Add(HygienstSealantFilterDescriptor);
                    HygienstSealantFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienstSealantFilter);
                }

                //Hygienist Custom Filter
                FilterDescriptor HygienistCustomFilterDescriptor = null;
                if (chkHygienistCustom.Checked && !string.IsNullOrEmpty(txtHygienistCustom.Text))
                {
                    HygienistCustomFilterDescriptor = new FilterDescriptor("G", FilterOperator.IsLike, "%" + txtHygienistCustom.Text + "%");
                    CompositeFilterDescriptor HygienistCustomFilter = new CompositeFilterDescriptor();
                    HygienistCustomFilter.FilterDescriptors.Add(HygienistCustomFilterDescriptor);
                    HygienistCustomFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(HygienistCustomFilter);
                }

                #endregion

                #region ComplexPatient Filter

                //ComplexPatient Filter
                FilterDescriptor ComplexPatientFilterDescriptor = null;
                if (chkComplexPatient.Checked)
                {
                    ComplexPatientFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsNotLike, "");
                    CompositeFilterDescriptor ComplexPatientFilter = new CompositeFilterDescriptor();
                    ComplexPatientFilter.FilterDescriptors.Add(ComplexPatientFilterDescriptor);
                    ComplexPatientFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientFilter);
                }

                FilterDescriptor ComplexPatientBehaviorFilterDescriptor = null;
                if (chkComplexPatientBehavior.Checked)
                {
                    ComplexPatientBehaviorFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%Bhvr%");

                    CompositeFilterDescriptor ComplexPatientBehaviorFilter = new CompositeFilterDescriptor();
                    ComplexPatientBehaviorFilter.FilterDescriptors.Add(ComplexPatientBehaviorFilterDescriptor);
                    ComplexPatientBehaviorFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientBehaviorFilter);
                }

                FilterDescriptor ComplexPatientAnestheticFilterDescriptor = null;
                if (chkComplexPatientAnesthetic.Checked)
                {
                    ComplexPatientAnestheticFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%Anes%");

                    CompositeFilterDescriptor ComplexPatientAnestheticFilter = new CompositeFilterDescriptor();
                    ComplexPatientAnestheticFilter.FilterDescriptors.Add(ComplexPatientAnestheticFilterDescriptor);
                    ComplexPatientAnestheticFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientAnestheticFilter);
                }

                FilterDescriptor ComplexPatientLimitedOpeningFilterDescriptor = null;
                if (chkComplexPatientLimitedOpening.Checked)
                {
                    ComplexPatientLimitedOpeningFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%LTD Open%");

                    CompositeFilterDescriptor ComplexPatientLimitedOpeningFilter = new CompositeFilterDescriptor();
                    ComplexPatientLimitedOpeningFilter.FilterDescriptors.Add(ComplexPatientLimitedOpeningFilterDescriptor);
                    ComplexPatientLimitedOpeningFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientLimitedOpeningFilter);
                }

                FilterDescriptor ComplexPatientGagReflexFilterDescriptor = null;
                if (chkComplexPatientGagReflex.Checked)
                {
                    ComplexPatientGagReflexFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%Gag%");

                    CompositeFilterDescriptor ComplexPatientGagReflexFilter = new CompositeFilterDescriptor();
                    ComplexPatientGagReflexFilter.FilterDescriptors.Add(ComplexPatientGagReflexFilterDescriptor);
                    ComplexPatientGagReflexFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientGagReflexFilter);
                }

                FilterDescriptor ComplexPatientSpecialNeedsFilterDescriptor = null;
                if (chkComplexPatientSpecialNeeds.Checked)
                {
                    ComplexPatientSpecialNeedsFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%SplNeeds%");

                    CompositeFilterDescriptor ComplexPatientSpecialNeedsFilter = new CompositeFilterDescriptor();
                    ComplexPatientSpecialNeedsFilter.FilterDescriptors.Add(ComplexPatientSpecialNeedsFilterDescriptor);
                    ComplexPatientSpecialNeedsFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientSpecialNeedsFilter);
                }

                FilterDescriptor ComplexPatientWheelChairFilterDescriptor = null;
                if (chkComplexPatientWheelchair.Checked)
                {
                    ComplexPatientWheelChairFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%Whlchair%");

                    CompositeFilterDescriptor ComplexPatientWheelChairFilter = new CompositeFilterDescriptor();
                    ComplexPatientWheelChairFilter.FilterDescriptors.Add(ComplexPatientWheelChairFilterDescriptor);
                    ComplexPatientWheelChairFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientWheelChairFilter);
                }

                //Hygienist Custom Filter
                FilterDescriptor ComplexPatientCustomFilterDescriptor = null;
                if (chkComplexPatientCustom.Checked && !string.IsNullOrEmpty(txtComplexPatientCustom.Text))
                {
                    ComplexPatientCustomFilterDescriptor = new FilterDescriptor("Y", FilterOperator.IsLike, "%" + txtComplexPatientCustom.Text + "%");
                    CompositeFilterDescriptor ComplexPatientCustomFilter = new CompositeFilterDescriptor();
                    ComplexPatientCustomFilter.FilterDescriptors.Add(ComplexPatientCustomFilterDescriptor);
                    ComplexPatientCustomFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(ComplexPatientCustomFilter);
                }
                #endregion

                #region After School Filter

                //After School Filter
                FilterDescriptor AfterSchoollFilterDescriptor = null;
                if (chkAfterSchool.Checked)
                {
                    AfterSchoollFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsNotLike, "");
                    CompositeFilterDescriptor AfterSchoolFilter = new CompositeFilterDescriptor();
                    AfterSchoolFilter.FilterDescriptors.Add(AfterSchoollFilterDescriptor);
                    AfterSchoolFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(AfterSchoolFilter);
                }

                FilterDescriptor AfterSchoolHygFilterDescriptor = null;
                if (chkAfterSchoolHyg.Checked)
                {
                    if (string.IsNullOrEmpty(txtAfterSchoolHyg.Text))
                        AfterSchoolHygFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%Hyg%");
                    else
                        AfterSchoolHygFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%Hyg-" + txtAfterSchoolHyg.Text + "%");

                    CompositeFilterDescriptor AfterSchoolHygFilter = new CompositeFilterDescriptor();
                    AfterSchoolHygFilter.FilterDescriptors.Add(AfterSchoolHygFilterDescriptor);
                    AfterSchoolHygFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(AfterSchoolHygFilter);
                }

                FilterDescriptor AfterSchoolDDSFilterDescriptor = null;
                if (chkAfterSchoolDDS.Checked)
                {
                    if (string.IsNullOrEmpty(txtAfterSchoolDDS.Text))
                        AfterSchoolDDSFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%DDS%");
                    else
                        AfterSchoolDDSFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%DDS-" + txtAfterSchoolHyg.Text + "%");

                    CompositeFilterDescriptor AfterSchoolDDSFilter = new CompositeFilterDescriptor();
                    AfterSchoolDDSFilter.FilterDescriptors.Add(AfterSchoolDDSFilterDescriptor);
                    AfterSchoolDDSFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(AfterSchoolDDSFilter);
                }

                FilterDescriptor AfterSchoolReferralConsultFilterDescriptor = null;
                if (chkAfterSchoolReferralConsult.Checked)
                {
                    AfterSchoolReferralConsultFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%REF%");

                    CompositeFilterDescriptor AfterSchoolReferralConsultFilter = new CompositeFilterDescriptor();
                    AfterSchoolReferralConsultFilter.FilterDescriptors.Add(AfterSchoolReferralConsultFilterDescriptor);
                    AfterSchoolReferralConsultFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(AfterSchoolReferralConsultFilter);
                }

                FilterDescriptor AfterSchoolCustomFilterDescriptor = null;
                if (chkAfterSchoolCustom.Checked && !string.IsNullOrEmpty(txtAfterSchoolCustom.Text))
                {
                    AfterSchoolCustomFilterDescriptor = new FilterDescriptor("B", FilterOperator.IsLike, "%" + txtAfterSchoolCustom.Text + "%");
                    CompositeFilterDescriptor AfterSchoolCustomFilter = new CompositeFilterDescriptor();
                    AfterSchoolCustomFilter.FilterDescriptors.Add(AfterSchoolCustomFilterDescriptor);
                    AfterSchoolCustomFilter.LogicalOperator = FilterLogicalOperator.Or;
                    mainfilterSquares.FilterDescriptors.Add(AfterSchoolCustomFilter);
                }

                #endregion


                CompositeFilterDescriptor mainfilter = new CompositeFilterDescriptor();


                //School Year Filter
                FilterDescriptor schoolYearFilterDescriptor = new FilterDescriptor("ActiveDate", FilterOperator.IsEqualTo, "8/1/2019");
                CompositeFilterDescriptor schoolYearFilter = new CompositeFilterDescriptor();
                schoolYearFilter.FilterDescriptors.Add(schoolYearFilterDescriptor);
                schoolYearFilter.LogicalOperator = FilterLogicalOperator.And;

                mainfilter.FilterDescriptors.Add(schoolYearFilter);

                //Treatment Complete Filter
                FilterDescriptor TreatmentCompleteFilterDescriptor = null;
                bool bAddedTreatmentCompleteFilter = false;
                switch (cmbStatus.SelectedIndex)
                {
                    case 0:
                        TreatmentCompleteFilterDescriptor = new FilterDescriptor("completion_date", FilterOperator.IsNull, "1/1/1980");
                        bAddedTreatmentCompleteFilter = true;
                        break;
                    case 1:
                        TreatmentCompleteFilterDescriptor = new FilterDescriptor("completion_date", FilterOperator.IsNotNull, "1/1/1980");
                        bAddedTreatmentCompleteFilter = true;
                        break;
                    case 2:
                        break;
                }

                if (bAddedTreatmentCompleteFilter)
                {
                    CompositeFilterDescriptor TreatmentCompleteFilter = new CompositeFilterDescriptor();
                    TreatmentCompleteFilter.FilterDescriptors.Add(TreatmentCompleteFilterDescriptor);
                    TreatmentCompleteFilter.LogicalOperator = FilterLogicalOperator.And;

                    mainfilter.FilterDescriptors.Add(TreatmentCompleteFilter);
                }

                //Grade Filter
                FilterDescriptor GradeFilterDescriptor = null;
                CompositeFilterDescriptor gradeFilter = new CompositeFilterDescriptor();
                gradeFilter.LogicalOperator = FilterLogicalOperator.Or;

                if (!chkGradeAll.Checked)
                {
                    if (chkGradeK.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "0");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }

                    if (chkGrade1st.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "1");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }
                    if (chkGrade2nd.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "2");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }
                    if (chkGrade3rd.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "3");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }
                    if (chkGrade4th.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "4");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }
                    if (chkGrade5th.Checked)
                    {

                        GradeFilterDescriptor = new FilterDescriptor("Grade", FilterOperator.IsEqualTo, "5");
                        gradeFilter.FilterDescriptors.Add(GradeFilterDescriptor);
                    }
                    if (gradeFilter.FilterDescriptors.Count > 0)
                        mainfilter.FilterDescriptors.Add(gradeFilter);
                }
                this.radGridView1.FilterDescriptors.Add(mainfilter);
                this.radGridView1.FilterDescriptors.Add(mainfilterSquares);
                pnlStudentPickList.HeaderText = "Student Pick List - (" + radGridView1.MasterView.Rows.Count + ")";
                lblStudentCount.Text = "Student Pick List - (" + radGridView1.MasterView.Rows.Count + ")";
        }

        private void RefreshSchools()
        {
            ResetTimer();

            m_nCurrentSchoolIndex = -1;

            string strAccessConnection = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=" + ConfigurationManager.AppSettings["DatabasePath"].ToString() + "; Jet OLEDB:Database Password=SDFDENTAL;Mode=Share Exclusive;Persist Security Info=True;";
            strAccessConnection += "Jet OLEDB:System Database=" + Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Access\system.mdw";

            OleDbConnection connect = new OleDbConnection();
            connect.ConnectionString = strAccessConnection;
            connect.Open();

            lblDataRefreshTime.Text = "Refreshing Schools";
            lblDataRefreshTime.Refresh();

            //Trucnate Table
            string QueryText = "Delete from Schools";
            using (OleDbCommand command = new OleDbCommand(QueryText, connect))
            {
                try
                {
                    //OleDbDataAdapter da = new OleDbDataAdapter(QueryText, connect);

                    command.ExecuteNonQuery();
                    connect.Close();
                }
                catch (Exception ex)
                {
                    connect.Close();
                }
            }

            string selectSQL = "";
            selectSQL = "Select * from [dbo].[vw_Schools]";
            DataTable gv2 = null;
            if (selectSQL != "")
            {
                gv2 = GetSqlDataTable(ConfigurationManager.ConnectionStrings["DMTSQLConnectionString"].ConnectionString, selectSQL);
                if (gv2.Rows.Count > 0)
                {
                    connect.Open();
                    try
                    {
                        foreach (DataRow dr in gv2.Rows)
                        {
                            QueryText = "INSERT INTO Schools (SchoolID, SchoolName) values (SchoolIDValue, SchoolNameValue)";
                            QueryText = QueryText.Replace("SchoolIDValue", "'" + dr["ID"].ToString() + "'");
                            QueryText = QueryText.Replace("SchoolNameValue", "'" + dr["Name"].ToString().Replace("'", "''") + "'");
                            using (OleDbCommand command = new OleDbCommand(QueryText, connect))
                            {
                                try
                                {

                                    command.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        connect.Close();
                        this.schoolsTableAdapter.Fill(this.schoolsDataset.Schools);
                    }
                }
            }
            m_nCurrentSchoolIndex = 1;
        }

        private void RefreshStudents()
        {
            Hashtable htStudents = new Hashtable();

            lblDataRefreshTime.Text = "Refreshing Students";
            lblDataRefreshTime.Refresh();

            if (cmbSchools.SelectedItem != null)
            {
                string selectSQL = "";
                selectSQL = "Select * from [dbo].[vw_ToothPick] where StudentID is not null and StudentID != '' and  school_id = " + cmbSchools.SelectedItem.Value + " and active_date = '2019-08-01' order by StudentID, sort_order";
                DataTable gv2 = null;
                if (selectSQL != "")
                {
                    gv2 = GetSqlDataTable(ConfigurationManager.ConnectionStrings["DMTSQLConnectionString"].ConnectionString, selectSQL);
                    if (gv2.Rows.Count > 0)
                    {
                        try
                        {
                            
                            Student student = new Student();

                            string strStudentID = "";
                            string strFinaleRedComment = "";

                            foreach (DataRow dr in gv2.Rows)
                            {
                                if (strStudentID != dr["StudentID"].ToString())
                                {
                                    if (!string.IsNullOrEmpty(strStudentID))
                                        htStudents.Add(student.StudentID, student);
                                    student = new Student();
                                }
                                strStudentID = dr["StudentID"].ToString();
                                student.StudentID = strStudentID;
                                student.StudentFirstName = dr["StudentFirstName"].ToString().Replace("'", "''");
                                student.StudentLastName = dr["StudentLastName"].ToString().Replace("'", "''");
                                if (student.StudentLastName.ToLower() == "carbajal" )
                                {
                                    Console.Write("1234");
                                }

                                    student.TeacherFirstName = dr["TeacherFirstName"].ToString().Replace("'", "''");
                                student.TeacherLastName = dr["TeacherLastName"].ToString().Replace("'", "''");
                                student.Language = dr["Language"].ToString().Replace("'", "''");
                                student.StudentDOB = dr["StudentDOB"].ToString().Replace("'", "''");
                                student.StudentGender = dr["StudentGender"].ToString().Replace("'", "''");
                                student.Grade = dr["Grade"].ToString().Replace("'", "''");
                                student.HomeRoom = dr["HomeRoom"].ToString().Replace("'", "''");
                                student.SpecialTime = dr["SpecialTime"].ToString().Replace("'", "''");
                                student.RecessTime = dr["RecessTime"].ToString().Replace("'", "''");
                                student.LunchTime = dr["LunchTime"].ToString().Replace("'", "''");
                                student.Comments = dr["Comments"].ToString().Replace("'", "''");
                                student.SchoolID = dr["school_id"].ToString().Replace("'", "''");

                                if (string.IsNullOrEmpty(dr["active_date"].ToString()))
                                    student.active_date = null;
                                else
                                    student.active_date =  dr["active_date"].ToString().Replace("'", "''");

                                if (string.IsNullOrEmpty(dr["treatment_accepted"].ToString()))
                                    student.treatment_accepted = null;
                                else
                                    student.treatment_accepted = dr["treatment_accepted"].ToString().Replace("'", "''").Replace("True", "1").Replace("False", "0");

                                string strColor = dr["Color"].ToString();
                                if (strColor.ToLower() == "red")
                                {
                                    string strAbbrev = dr["abbrev"].ToString();


                                    if (!string.IsNullOrEmpty(strAbbrev))
                                    {
                                        if (!string.IsNullOrEmpty(student.RedText))
                                        {
                                            student.RedText += "\n";
                                        }
                                        student.RedText += strAbbrev;
                                        string strExtraData = dr["extra_data"].ToString();
                                        if (!string.IsNullOrEmpty(strExtraData))
                                        {
                                            student.RedText += "-" + strExtraData;
                                        }
                                    }


                                }
                                string strRedNote = dr["red_note"].ToString();
                                if (!string.IsNullOrEmpty(strRedNote))
                                {
                                    student.RedText += " " + strRedNote;
                                    student.RedText = student.RedText.Trim();
                                }

                                if (strColor.ToLower() == "yellow")
                                {
                                    string strAbbrev = dr["abbrev"].ToString();


                                    if (!string.IsNullOrEmpty(strAbbrev))
                                    {
                                        if (!string.IsNullOrEmpty(student.YellowText))
                                        {
                                            student.YellowText += "\n";
                                        }
                                        student.YellowText += strAbbrev;
                                        string strExtraData = dr["extra_data"].ToString();
                                        if (!string.IsNullOrEmpty(strExtraData))
                                        {
                                            student.YellowText += "-" + strExtraData;
                                        }
                                    }


                                }
                                string strYellowNote = dr["yellow_note"].ToString();
                                if (!string.IsNullOrEmpty(strYellowNote))
                                {
                                    student.YellowText += " " +  strYellowNote;
                                    student.YellowText = student.YellowText.Trim();
                                }


                                if (strColor.ToLower() == "blue")
                                {
                                    string strAbbrev = dr["abbrev"].ToString();


                                    if (!string.IsNullOrEmpty(strAbbrev))
                                    {
                                        if (!string.IsNullOrEmpty(student.BlueText))
                                        {
                                            student.BlueText += "\n";
                                        }
                                        student.BlueText += strAbbrev;
                                        string strExtraData = dr["extra_data"].ToString();
                                        if (!string.IsNullOrEmpty(strExtraData))
                                        {
                                            student.BlueText += "-" + strExtraData;
                                        }
                                    }
                                }

                                string strBlueNote = dr["blue_note"].ToString();
                                if (!string.IsNullOrEmpty(strBlueNote))
                                {
                                    student.BlueText += " " + strBlueNote;
                                    student.BlueText = student.BlueText.Trim();
                                }


                                if (strColor.ToLower() == "green")
                                {
                                    string strAbbrev = dr["abbrev"].ToString();


                                    if (!string.IsNullOrEmpty(strAbbrev))
                                    {
                                        if (!string.IsNullOrEmpty(student.GreenText))
                                        {
                                            student.GreenText += "\n";
                                        }
                                        student.GreenText += strAbbrev;
                                        string strExtraData = dr["extra_data"].ToString();
                                        if (!string.IsNullOrEmpty(strExtraData))
                                        {
                                            student.GreenText += "-" + strExtraData;
                                        }
                                    }
                                }

                                string strGreenNote = dr["green_note"].ToString();
                                if (!string.IsNullOrEmpty(strGreenNote))
                                {
                                    student.GreenText += " " + strGreenNote;
                                    student.GreenText = student.GreenText.Trim();
                                }


                                if (string.IsNullOrEmpty(dr["completion_date"].ToString()))
                                    student.completion_date = null;
                                else
                                    student.completion_date = dr["completion_date"].ToString().Replace("'", "''");
                            }
                            //Add the last student to the list
                            if (!string.IsNullOrEmpty(strStudentID))
                                htStudents.Add(student.StudentID, student);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                WriteStudentsTable(htStudents);
                this.studentTableTableAdapter.Fill(this.studentDataSet.StudentTable);
                radGridView1.Refresh();
                ApplyFilteringtoGrid();
            }
        }

        private void WriteStudentsTable(Hashtable htStudents)
        {
            ResetTimer();

            string strAccessConnection = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=" + ConfigurationManager.AppSettings["DatabasePath"].ToString() + "; Jet OLEDB:Database Password=SDFDENTAL;Mode=Share Exclusive;Persist Security Info=True;";
            strAccessConnection += "Jet OLEDB:System Database=" + Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Access\system.mdw";

            OleDbConnection connect = new OleDbConnection();
            connect.ConnectionString = strAccessConnection;
            connect.Open();

            //Trucnate Table
            string QueryText = "Delete from StudentTable";
            using (OleDbCommand command = new OleDbCommand(QueryText, connect))
            {
                try
                {
                    //OleDbDataAdapter da = new OleDbDataAdapter(QueryText, connect);

                    command.ExecuteNonQuery();
                    connect.Close();
                }
                catch (Exception ex)
                {
                    connect.Close();
                }
            }


            connect.Open();
            try {
                if (htStudents != null)
                {
                    foreach (DictionaryEntry dct in htStudents)
                    {
                        QueryText = "INSERT INTO StudentTable (StudentID,StudentFirstName,StudentLastName,TeacherFirstName,TeacherLastName,[Language],DOB,Gender,Grade,HomeRoom,SpecialTime,RecessTime,LunchTime,Comments,R,G,Y,B,SchoolID,ActiveDate,treatment_accepted,completion_date, status) values (StudentIDValue,StudentFirstNameValue,StudentLastNameValue,TeacherFirstNameValue,TeacherLastNameValue,LanguageValue,DOBValue,GenderValue,GradeValue,HomeRoomValue,SpecialTimeValue,RecessTimeValue,LunchTimeValue,CommentsValue,RValue,GValue,YValue,BValue,SchoolIDValue,active_dateValue,treatment_acceptedValue,CompletionDateValue,' Available')";
                        QueryText = QueryText.Replace("StudentIDValue", "'" + ((Student)dct.Value).StudentID.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("StudentFirstNameValue", "'" + ((Student)dct.Value).StudentFirstName.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("StudentLastNameValue", "'" + ((Student)dct.Value).StudentLastName.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("TeacherFirstNameValue", "'" + ((Student)dct.Value).TeacherFirstName.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("TeacherLastNameValue", "'" + ((Student)dct.Value).TeacherLastName.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("LanguageValue", "'" + ((Student)dct.Value).Language.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("DOBValue", "'" + ((Student)dct.Value).StudentDOB.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("GenderValue", "'" + ((Student)dct.Value).StudentGender.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("GradeValue", "'" + ((Student)dct.Value).Grade.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("HomeRoomValue", "'" + ((Student)dct.Value).HomeRoom.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("SpecialTimeValue", "'" + ((Student)dct.Value).SpecialTime.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("RecessTimeValue", "'" + ((Student)dct.Value).RecessTime.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("LunchTimeValue", "'" + ((Student)dct.Value).LunchTime.Replace("'", "''") + "'");
                        QueryText = QueryText.Replace("CommentsValue", "'" + ((Student)dct.Value).Comments.Replace("'", "''") + "'");

                        string strRedText = "";
                        if (string.IsNullOrEmpty(((Student)dct.Value).RedNote))
                        {
                            strRedText = ((Student)dct.Value).RedText;
                        }
                        else
                        {
                            strRedText = ((Student)dct.Value).RedText + "\n" + ((Student)dct.Value).RedNote;
                        }

                        if (strRedText != null)
                            strRedText = strRedText.Replace("'", "''");

                        string strGreenText = ((Student)dct.Value).GreenText;
                        if (strGreenText != null)
                            strGreenText = strGreenText.Replace("'", "''");

                        string strYellowText = ((Student)dct.Value).YellowText;
                        if (strYellowText != null)
                            strYellowText = strYellowText.Replace("'", "''");


                        string strBlueText = ((Student)dct.Value).BlueText;
                        if (strBlueText != null)
                            strBlueText = strBlueText.Replace("'", "''");

                        QueryText = QueryText.Replace("RValue", "'" + strRedText + "'");

                        QueryText = QueryText.Replace("GValue", "'" + strGreenText + "'");

                        QueryText = QueryText.Replace("YValue", "'" + strYellowText + "'");

                        QueryText = QueryText.Replace("BValue", "'" + strBlueText + "'");

                        QueryText = QueryText.Replace("SchoolIDValue", "'" + ((Student)dct.Value).SchoolID + "'");
                        QueryText = QueryText.Replace("active_dateValue", "'" + ((Student)dct.Value).active_date + "'");
                        QueryText = QueryText.Replace("treatment_acceptedValue", "'" + ((Student)dct.Value).treatment_accepted + "'");
                        //if (((Student)dct.Value).completion_date == null)
                        //{
                        //    ((Student)dct.Value).completion_date = "1/1/1980  12:00:00 AM";
                        //    ((Student)dct.Value).completion_date = null;
                        //}
                        if (string.IsNullOrEmpty(((Student)dct.Value).completion_date))
                            QueryText = QueryText.Replace("CompletionDateValue", "null");
                        else
                            QueryText = QueryText.Replace("CompletionDateValue", "'" + ((Student)dct.Value).completion_date.ToString().Replace("'", "''") + "'");

                        using (OleDbCommand command = new OleDbCommand(QueryText, connect))
                        {
                            try
                            {

                                command.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }

                    }
                }
                else
                {
                    radGridView1.ClearSelection();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connect.Close();
            }

        }

        public static DataTable GetSqlDataTable(string connString, string query)
        {
            SqlConnection conn = new SqlConnection(connString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(query, conn);

            DataTable myDataTable = new DataTable();

            
            try
            {
                conn.Open();
                adapter.Fill(myDataTable);
            }catch (Exception ex)
            {
                MessageBox.Show("Unae to Connect to Database. -  " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return myDataTable;
        }

        private void SortGrid()
        {
            ResetTimer();

            btnSort1.Image = Resources.AtoZ;
            btnSort2.Image = Resources.AtoZ;
            btnSort3.Image = Resources.AtoZ;
            foreach (GridViewColumn g in this.radGridView1.Columns)
            {
                g.SortOrder = RadSortOrder.None;
            }
            string strValue = "";
            if (ddlSort1.SelectedItem != null && ddlSort1.SelectedItem.Text != "")
            {
                strValue = GetColumnNameFromText(ddlSort1.SelectedItem.Text);
                if (btnSort1.IsChecked)
                {
                    btnSort1.Image = Resources.ZtoA;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Descending;
                }
                else
                {
                    btnSort1.Image = Resources.AtoZ;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Ascending;

                }
            }

            if (ddlSort2.SelectedItem != null && ddlSort2.SelectedItem.Text != "")
            {
                strValue = GetColumnNameFromText(ddlSort2.SelectedItem.Text);
                if (btnSort2.IsChecked)
                {
                    btnSort2.Image = Resources.ZtoA;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Descending;
                }
                else
                {
                    btnSort2.Image = Resources.AtoZ;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Ascending;
                }
            }

            if (ddlSort3.SelectedItem != null && ddlSort3.SelectedItem.Text != "")
            {
                strValue = GetColumnNameFromText(ddlSort3.SelectedItem.Text);
                if (btnSort3.IsChecked)
                {
                    btnSort3.Image = Resources.ZtoA;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Descending;
                }
                else
                {
                    btnSort3.Image = Resources.AtoZ;
                    this.radGridView1.Columns[strValue].SortOrder = RadSortOrder.Ascending;
                }
            }
        }

        private string GetColumnNameFromText(string strColumnText)
        {
            string strColumnValue = "";

            foreach (GridViewColumn g in this.radGridView1.Columns)
            {
                if (g.HeaderText == strColumnText)
                {
                    return g.FieldName;
                }
            }

            return strColumnValue;
        }



        private void UpdateStudentStatus(long studentID, string strValue)
        {
            string strAccessConnection = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source=" + ConfigurationManager.AppSettings["DatabasePath"].ToString() + "; Jet OLEDB:Database Password=SDFDENTAL;Mode=Share Exclusive;Persist Security Info=True;";
            strAccessConnection += "Jet OLEDB:System Database=" + Environment.GetEnvironmentVariable("APPDATA") + @"\Microsoft\Access\system.mdw";

            OleDbConnection connect = new OleDbConnection();
            connect.ConnectionString = strAccessConnection;
            connect.Open();

            string selectSQL = "";
            selectSQL = "Update StudentTable SET ";
            selectSQL = selectSQL + "Status='" + strValue;
            selectSQL = selectSQL + "' Where Studentid='" + studentID + "'";

            if (selectSQL != "")
            {
                try
                {
                    OleDbCommand command = new OleDbCommand(selectSQL, connect);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        #endregion

        private void pnlSchoolYear_Expanded(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void pnlSchoolYear_Collapsed(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void pnlChildSearch_Expanded(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void pnlChildSearch_Collapsed(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void pnlStudentPickList_Expanded(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void pnlStudentPickList_Collapsed(object sender, EventArgs e)
        {
            new Thread(() => { resizeMe(); }).Start();
        }

        private void resizeMe()
        {
            Thread.Sleep(100);

            this.BeginInvoke((Action)(() => {

                doIt();

            }));
        }

        private void doIt()
        {
            pnlStudentPickList.Height = 100;
            pnlStudentPickList.Width = 100;
        }
    }
}

public class Student
{
    private string m_StudentID;

    private string m_StudentFirstName;
    private string m_StudentLastName;
    private string m_TeacherFirstName;
    private string m_TeacherLastName;
    private string m_Language;
    private string m_StudentDOB;
    private string m_StudentGender;
    private string m_Grade;
    private string m_HomeRoom;
    private string m_SpecialTime;
    private string m_RecessTime;
    private string m_LunchTime;
    private string m_Comments;
    private string m_SchoolID;
    private string m_active_date;
    private string m_treatment_accepted;
    private string m_completion_date;

    private string m_RedText;
    private string m_YellowText;
    private string m_GreenText;
    private string m_BlueText;

    private string m_RedNote;
    private string m_YellowNote;
    private string m_GreenNote;
    private string m_BlueNote;

    public string StudentID
    {
        set
        {
            m_StudentID = value;
        }
        get
        {
            return m_StudentID;
        }
    }

    public string SchoolID
    {
        set
        {
            m_SchoolID = value;
        }
        get
        {
            return m_SchoolID;
        }
    }
    public string StudentFirstName
    {
        set
        {
            m_StudentFirstName = value;
        }
        get
        {
            return m_StudentFirstName;
        }
    }
    public string StudentLastName
    {
        set
        {
            m_StudentLastName = value;
        }
        get
        {
            return m_StudentLastName;
        }
    }
    public string TeacherFirstName
    {
        set
        {
            m_TeacherFirstName = value;
        }
        get
        {
            return m_TeacherFirstName;
        }
    }
    public string TeacherLastName
    {
        set
        {
            m_TeacherLastName = value;
        }
        get
        {
            return m_TeacherLastName;
        }
    }
    public string Language
    {
        set
        {
            m_Language = value;
        }
        get
        {
            return m_Language;
        }
    }
    public string StudentDOB
    {
        set
        {
            m_StudentDOB = value;
        }
        get
        {
            return m_StudentDOB;
        }
    }
    public string StudentGender
    {
        set
        {
            m_StudentGender = value;
        }
        get
        {
            return m_StudentGender;
        }
    }
    public string Grade
    {
        set
        {
            m_Grade = value;
        }
        get
        {
            return m_Grade;
        }
    }
    public string HomeRoom
    {
        set
        {
            m_HomeRoom = value;
        }
        get
        {
            return m_HomeRoom;
        }
    }
    public string SpecialTime
    {
        set
        {
            m_SpecialTime = value;
        }
        get
        {
            return m_SpecialTime;
        }
    }

    public string RecessTime
    {
        set
        {
            m_RecessTime = value;
        }
        get
        {
            return m_RecessTime;
        }
    }

    public string LunchTime
    {
        set
        {
            m_LunchTime = value;
        }
        get
        {
            return m_LunchTime;
        }
    }
    public string Comments
    {
        set
        {
            m_Comments = value;
        }
        get
        {
            return m_Comments;
        }
    }
    public string active_date
    {
        set
        {
            m_active_date = value;
        }
        get
        {
            return m_active_date;
        }
    }
    public string treatment_accepted
    {
        set
        {
            m_treatment_accepted = value;
        }
        get
        {
            return m_treatment_accepted;
        }
    }
    public string completion_date
    {
        set
        {
            m_completion_date = value;
        }
        get
        {
            return m_completion_date;
        }
    }

    public string RedText
    {
        set
        {
            m_RedText = value;
        }
        get
        {
            return m_RedText;
        }
    }
    public string RedNote
    {
        set
        {
            m_RedNote = value;
        }
        get
        {
            return m_RedNote;
        }
    }
    public string YellowText
    {
        set
        {
            m_YellowText = value;
        }
        get
        {
            return m_YellowText;
        }
    }
    public string YellowNote
    {
        set
        {
            m_YellowNote = value;
        }
        get
        {
            return m_YellowNote;
        }
    }
    public string GreenText
    {
        set
        {
            m_GreenText = value;
        }
        get
        {
            return m_GreenText;
        }
    }
    public string GreenNote
    {
        set
        {
            m_GreenNote = value;
        }
        get
        {
            return m_GreenNote;
        }
    }
    public string BlueText
    {
        set
        {
            m_BlueText = value;
        }
        get
        {
            return m_BlueText;
        }
    }
    public string BlueNote
    {
        set
        {
            m_BlueNote = value;
        }
        get
        {
            return m_BlueNote;
        }
    }
}